var elixir = require('laravel-elixir');
var gulp = require("gulp");
var exec = require("child_process").exec;
/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function (mix){
   mix.rubySass('app.scss');
   mix.scripts([
      //"lib/jquery/jquery.js",
      "lib/mdl/material.js",
   ], "public/js/common.js");
   mix.scripts([
      "lib/mdl/material.js",
      "lib/angular/*.js",
      "lib/plugins/*.js",
      "app/**/*.module.js",
      "app/**/!(*.module.js|*.spec.js)"
   ], "public/js/app.js");
});

gulp.task("launch", function(){
   exec('browser-sync start --proxy=hazel.dev --files="resources/views/**/*, public/**/*"');
})