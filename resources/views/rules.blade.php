@extends('app')

@section('content')
	<div ng-app="app" class="grid-container-wrapper phone-grid">
		<div ng-controller="QuestionsController as vm" class="container grid-container">
			<div class="col-md-12 grid-title-container">
				<h1 class="text-center">Your Device: {{$phone->name}}</h1>
			</div>
			<offer-card alias="vm.offerCard" id="final_offer_card" ng-hide="vm.editMode" class="final-offer-card" enter-edit-mode="vm.editMode=true" on-offer-grabbed="vm.grabOffer(offerData)"></offer-card>
			<offer-form alias="vm.offerForm"></offer-form>
			<div ng-show="vm.editMode" style="min-height:500px;" class="col-md-10 col-md-offset-1 mdl-grid mdl-card mdl-card mdl-shadow--2dp">
				<div style="" class="mdl-cell mdl-cell--4-col">
					<div class="mdl-card__title mdl-card--border hidden">
						<div class="mdl-card__title-text">{{$phone->name}}</div>
					</div>
					<div style="height:400px;margin-top:13px;" class="mdl-card__media custom md-card--border">
						<img src="{{ $phone->img_url }}" class="grid-item-img">
					</div>
					<div style="margin: 30px auto;text-align:center;" class="mdl-card__supporting-text">
						<span style="font-size: 25px; font-weight: 500; color: #777; line-height: 70px;" class="est-offer-value">Est. Offer: $@{{vm.estimatedOffer}}</span><br>
						<button ng-disabled="!vm.finalCalculateButtonEnabled" ng-click="vm.calculateFinalOffer()" class="mdl-button mdl-js-button mdl-button--raised mdl-button--accent mdl-js-ripple-effect">Calculate Final Offer</button>
					</div>
				</div>
				<div style="" class="mdl-cell mdl-cell--8-col">
					<div class="mdl-card__title mdl-card--border">
						<div class="mdl-card__title-text">Let us know little more about your device: </div>
					</div>
					<div class="questions-form">
						<question ng-repeat="question in vm.questions" on-choice-changed="vm.handleQuestionAnswered(question)" data="question"></question>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
		var phoneDataFromServer = {!! $phone !!};
	</script>

@endsection