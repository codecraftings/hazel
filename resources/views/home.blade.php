@extends('app')
@section('page_class','home')
@section('content')

    <div class="slider-section">
        <div class="container">
            <div class="col-sm-5 hidden-xs landing-img-container">
                {{--<img class="landing-img" src="{{url('images/bg5.png')}}"/>--}}
            </div>
            <div class="col-sm-7 landing-slogan">
                <span class="text">We Pay The Most For Broken Phones</span><br>
                <button
                        class="flat-btn1 landing-button"
                        onclick="top.location='{{ url('brands/1') }}'">Sell Your iPhone Now
                </button>
                <br>
                <button
                        class="flat-btn1 landing-button"
                        onclick="top.location='{{ url('brands') }}'">Sell Your Other Phones
                </button>
            </div>
        </div>
    </div>
    <div id="feat-list">
        <div class="container">
            <div class="col-xs-4 featbox feat1 item-centered">
                <div class="h-centered">
                    <div class="inner">
                        <div class="feat-icon"><i class="material-icons">local_shipping</i></div>
                        <div class="text">Free Shipping</div>
                    </div>
                </div>
            </div>
            <div class="col-xs-4 featbox feat2 item-centered">
                <div class="h-centered">
                    <div class="inner">
                        <div class="feat-icon"><i class="material-icons">verified_user</i></div>
                        <div class="text">30 Days Price Guarantee</div>
                    </div>
                </div>
            </div>
            <div class="col-xs-4 featbox feat3 item-centered">
                <div class="h-centered">
                    <div class="inner">
                        <div class="feat-icon"><i class="material-icons">payment</i></div>
                        <div class="text">Quick Payment</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="">
        <div class="container headline-container">
            <h1 class="text-center">How It Works</h1>

            <div class="headline-border col-md-offset-5"></div>
        </div>
        <div class="container middle-container">
            <div class="row">

                <div class="timeline-border col-md-offset-6">

                </div>
                <div class="col-md-8 col-md-offset-2 full-box-container">
                    <div class="col-md-6 box-icon-container">
                        <i class="material-icons">store</i>
                    </div>
                    <div class="col-md-6 box-text-container">
                        <h2 class="text-left">Get instant quote</h2>

                        <p class="text-left">Find your phone from our catelog. Answer questions about your device
                            conditions
                            and get our offer value instantly.</p>
                        <span class="step-circle">1</span>
                    </div>
                </div>
                <div class="col-md-8 col-md-offset-2 full-box-container">
                    <div class="col-md-6 box-text-container">
                        <h2 class="text-left">Print our label</h2>

                        <p class="text-left">Once you register for our offer, we will send you a receipt to print and
                            send
                            with your device</p>
                        <span class="step-circle left">2</span>
                    </div>
                    <div class="col-md-6 box-icon-container">
                        <i class="material-icons">receipt</i>
                    </div>
                </div>
                <div class="col-md-8 col-md-offset-2 full-box-container">
                    <div class="col-md-6 box-icon-container">
                        <i class="material-icons">card_giftcard</i>
                    </div>
                    <div class="col-md-6 box-text-container">
                        <h2 class="text-left">Ship your device</h2>

                        <p class="text-left">Package your device. Attach printed label to the package and ship it to our
                            address.</p>
                        <span class="step-circle">3</span>
                    </div>
                </div>
                <div class="col-md-8 col-md-offset-2 full-box-container">
                    <div class="col-md-6 box-text-container">
                        <h2 class="text-left">Get Paid</h2>

                        <p class="text-left">As soon as we receive your device, we will make payment through your
                            preferred
                            payment method. We accept most payment methods.</p>
                        <span class="step-circle left">4</span>
                    </div>
                    <div class="col-md-6 box-icon-container">
                        <i class="material-icons">attach_money</i>
                    </div>
                </div>
            </div>
        </div>
        {{--
        <div class="container img-container">
            <div class="col-md-12">
                <div class="col-md-3 bottom-img-box">
                    <img src="{{ url('/images/demo/2.jpg') }}" class="bottom-img">

                    <div class="text-container"><p>"Great customer service. Got help instantly. Highly recomended!"</p>
                    </div>
                </div>
                <div class="col-md-3 bottom-img-box">
                    <img src="{{ url('/images/demo/3.png') }}" class="bottom-img">

                    <div class="text-container"><p>"Great customer service. Got help instantly. Highly recomended!"</p>
                    </div>
                </div>
                <div class="col-md-3 bottom-img-box">
                    <img src="{{ url('/images/demo/4.jpg') }}" class="bottom-img">

                    <div class="text-container"><p>"Great customer service. Got help instantly. Highly recomended!"</p>
                    </div>
                </div>
                <div class="col-md-3 bottom-img-box">
                    <img src="{{ url('/images/demo/5.jpg') }}" class="bottom-img">

                    <div class="text-container"><p>"Great customer service. Got help instantly. Highly recomended!"</p>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-3 bottom-img-box">
                    <img src="{{ url('/images/demo/6.jpg') }}" class="bottom-img">

                    <div class="text-container"><p>"Great customer service. Got help instantly. Highly recomended!"</p>
                    </div>
                </div>
                <div class="col-md-3 bottom-img-box">
                    <img src="{{ url('/images/demo/7.jpg') }}" class="bottom-img">

                    <div class="text-container"><p>"Great customer service. Got help instantly. Highly recomended!"</p>
                    </div>
                </div>
                <div class="col-md-3 bottom-img-box">
                    <img src="{{ url('/images/demo/8.jpg') }}" class="bottom-img">

                    <div class="text-container"><p>"Great customer service. Got help instantly. Highly recomended!"</p>
                    </div>
                </div>
                <div class="col-md-3 bottom-img-box">
                    <img src="{{ url('/images/demo/9.jpg') }}" class="bottom-img">

                    <div class="text-container"><p>"Great customer service. Got help instantly. Highly recomended!"</p>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-3 bottom-img-box">
                    <img src="{{ url('/images/demo/10.jpg') }}" class="bottom-img">

                    <div class="text-container"><p>"Great customer service. Got help instantly. Highly recomended!"</p>
                    </div>
                </div>
                <div class="col-md-3 bottom-img-box">
                    <img src="{{ url('/images/demo/11.jpg') }}" class="bottom-img">

                    <div class="text-container"><p>"Great customer service. Got help instantly. Highly recomended!"</p>
                    </div>
                </div>
                <div class="col-md-3 bottom-img-box">
                    <img src="{{ url('/images/demo/12.jpg') }}" class="bottom-img">

                    <div class="text-container"><p>"Great customer service. Got help instantly. Highly recomended!"</p>
                    </div>
                </div>
                <div class="col-md-3 bottom-img-box">
                    <img src="{{ url('/images/demo/3.png') }}" class="bottom-img">

                    <div class="text-container"><p>"Great customer service. Got help instantly. Highly recomended!"</p>
                    </div>
                </div>
            </div>
        </div>
        --}}
    </div>

@endsection
