@extends('app')
@section("window-title")
    Confirm Checkout
@stop
@section('content')
    <div ng-controller="CheckoutConfirmationController as vm" class="grid-container-wrapper">
        <div class="container">
            <div class="offer-details-card confirmation-page-container flat-grid-container mdl-card mdl-shadow--2dp">
                <div offer-details="vm.copiedOfferData"></div>
                <div class="alert alert-danger" ng-show="vm.error">@{{vm.error}}</div>
                <div ng-hide="vm.confirmed" class="alert alert-info">
                    <label for="checkbox-1" class="">
                        <input id="checkbox-1" type="checkbox" ng-model="vm.terms_accepted" class="">
                        </input>&nbsp;&nbsp;
                        <span class="mdl-checkbox__label">
                             By clicking this checkbox you agree to accept our <a href="{{url('pages/terms')}}"
                                                                            target="_blank">terms
                                and conditions</a>
                        </span>
                    </label>
                </div>
                <div ng-hide="vm.confirmed" class="buttons-group">
                    <button ng-click="vm.confirm()" ng-disabled="vm.confirming || !vm.terms_accepted"
                            class="mdl-button mdl-js-button mdl-button--colored mdl-button--raised mdl-js-ripple-effect confirm-btn">
                        Confirm Order
                    </button>
                    <a href="checkout#/step2" ng-hide="vm.confirming"
                       class="mdl-button mdl-js-button mdl-js-ripple-effect back-btn"><< Back</a>

                    <div ng-show="vm.confirming"
                         class="mdl-spinner mdl-spinner--single-color mdl-js-spinner is-active loader"></div>
                </div>
                <div class="alert alert-success" ng-show="vm.confirmed">
                    Thank you for accepting our offer. We will review your details and email you with shipping label and
                    other tracking details.
                </div>
                <div class="next-steps mdl-grid" ng-show="vm.confirmed">
                    <h3>What happens Next?</h3>

                    <div class="mdl-cell mdl-cell--4-col next-step-cell">
                        <i class="material-icons">system_update</i>

                        <h3>Prepare your phone</h3>
                        <ul>
                            <li>Turn off <a href="{{url('pages/findmyphone')}}">"Find my iPhone"</a></li>
                            <li>Deactivate your service</li>
                            <li>Remove data</li>
                        </ul>
                    </div>
                    <div class="mdl-cell mdl-cell--4-col next-step-cell">
                        <i class="material-icons">send</i>

                        <h3>Pack and send</h3>
                        <ul>
                            <li>Check email for pre-paid shipping label</li>
                            <li>Pack your device in a box</li>
                            <li>Drop it off at your <a>nearest FedEx location</a></li>
                        </ul>
                    </div>
                    <div class="mdl-cell mdl-cell--4-col next-step-cell">
                        <i class="material-icons">attach_money</i>

                        <h3>Get Paid</h3>
                        <ul>
                            <li>Once we receive an item it typically takes about a week for your Amazon.com Gift Card to
                                be emailed to you.
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop