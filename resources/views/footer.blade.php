<div class="footer-area">
    <div class="container footer-container">
        <div class="col-md-10 footer-menu-container">
            <nav class="navbar navbar-default footer-nav hidden-xs">
                <ul class="nav navbar-nav footer-menu-list">
                    <li><a href="/">Home</a></li>
                    <li><a href="pages/howitworks">About us</a></li>
                    <li><a href="pages/terms">Terms and conditions</a></li>
                    <li><a href="pages/support">Support Center</a></li>
                </ul>
            </nav>
            <div class="visible-xs"><br></div>
            <p class="copyright-text">&copy Copyright 2015. All rights reserved!</p>
        </div>
        <div class="col-md-2">
            <a class="navbar-brand footer-logo" href="http://hazel.dev">sellPhone</a>
        </div>
    </div>
</div>
<script type='text/javascript' id="__bs_script__">//<![CDATA[
    document.write("<script async src='http://HOST:3000/browser-sync/browser-sync-client.2.9.3.js'><\/script>".replace("HOST", location.hostname));
    //]]></script>