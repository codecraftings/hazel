@extends('app')
@section("window-title")
    Device Details
@stop
@section('content')
    <div class="grid-container-wrapper">
        <div ng-controller="QuestionsController as vm" class="container">
            <offer-card alias="vm.offerCard" id="final_offer_card" ng-hide="vm.editMode" class="final-offer-card"
                        enter-edit-mode="vm.editMode=true" on-offer-grabbed="vm.grabOffer(offerData)"></offer-card>
            <offer-form alias="vm.offerForm"></offer-form>
            <div ng-show="vm.editMode" style="padding: 15px;"
                 class="col-sm-10 col-xs-12 flat-grid-container col-sm-offset-1 mdl-card mdl-Egrid mdl-shadow--2dp">
                <div class="row">

                    <div class="mdl-Ecell mdl-Ecell--3-col col-sm-3 hidden-xs">
                        <div style="height:auto;margin-top:10px;text-align:center;" class="">
                            <img style="max-width:90%;" src="{{ $phone->img_url }}">
                        </div>
                        <div style="text-align:center;width:100%;font-size: 20px; font-weight: 300; color: #757575; margin-top: 20px;"
                             class="est-offer-value">{{$phone->name}}</div>
                        <div style="text-align:center;width:100%;font-size: 20px; font-weight: 500; color: #FF9800; line-height: normal;"
                             class="est-offer-value">Est. Offer: $@{{vm.estimatedOffer}}</div>
                        <br>
                    </div>
                    <div class="mdl-Ecell mdl-Ecell--9-col col-sm-9 flat-questions-container">
                        <div ng-show="vm.loaderVisible" style="height:70%;width:70%;" class="loader v-centered">
                            <div class="h-centered">
                                <div class="mdl-spinner mdl-spinner--single-color mdl-js-spinner is-active"></div>
                            </div>
                        </div>
                        <div class="q-card">
                            <div class="mdl-card__title mdl-card--border">
                                <div class="flat-q-header">
                                    <img ng-show="vm.currentQuestion.image" class="flat-q-img"
                                         ng-src="@{{ vm.currentQuestion.image|url }}"/>

                                    <h3 class="flat-q-title">@{{ vm.currentQuestion.title }}</h3>

                                    <p class="flat-q-explain"
                                       ng-show="vm.currentQuestion.explanation">@{{ vm.currentQuestion.explanation }}</p>
                                </div>
                            </div>
                            <div class="flat-grid opt-container col-md-12">
                                <div ng-repeat="opt in vm.currentQuestion.options"
                                     ng-click="vm.setSelectedOpt(vm.currentQuestion, opt)"
                                     ng-class="{'active':opt.selected, 'img':opt.image}" class="flat-option v-centered">
                                    <span ng-if="opt.icon" class="icon"><img ng-src="@{{ opt.icon|url }}"/></span>
                                    <span ng-if="opt.label&&!opt.image">@{{ opt.label }}</span>
                                    <img ng-if="opt.image" ng-src="@{{ opt.image|url }}"/>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="col-md-12 q-actions">
                            <button style="font-size:20px;" ng-click="vm.lastQuestion()"
                                    ng-show="vm.currentQuestion.index>1"
                                    class="mdl-button mdl-js-button mdl-button--colored mdl-js-ripple-effect fr hidden-sm hidden-xs">Back
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        var phoneDataFromServer = <?php echo $phone;?>;
    </script>
@endsection
