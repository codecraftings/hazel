<div class="offer-details-card mdl-grid">
    <table class="mdl-data-table mdl-js-data-table mdl-cell mdl-cell--12-col">
        <thead>
        <tr>
            <th class="mdl-data-table__cell--non-numeric">Offer Details</th>
            <th class="mdl-data-table__celld--non-numeric">Status: {{ucwords(str_replace("_"," ",$offer->status))}}</th>
        </tr>
        <tr>
            <th class="mdl-data-table__cell--non-numeric">Item</th>
            <th class="mdl-data-table__cell--non-numeric">Questions</th>
            <th class="">Trade-In Value</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td class="mdl-data-table__cell--non-numeric">Iphone 5</td>
            <td class="mdl-data-table__cell--non-numeric">
                <ul class="questions">
                    <li>What is condition? Good</li>
                    <li>What is condition? Good</li>
                </ul>
                <a class="more-btn" href="#">5 more</a>
            </td>
            <td>$400</td>
        </tr>
        <tr>
            <td class="mdl-data-table__cell--non-numeric">Iphone 5</td>
            <td class="mdl-data-table__cell--non-numeric">
                <ul class="questions">
                    <li>What is condition? Good</li>
                    <li>What is condition? Good</li>
                </ul>
                <a class="more-btn" href="#">5 more</a>
            </td>
            <td>$400</td>
        </tr>
        <tr>
            <td></td>
            <td>Total:</td>
            <td>$800</td>
        </tr>
        </tbody>
    </table>
    <div class="shipping-details mdl-card mdl-card--border mdl-cell mdl-cell--6-col">
        <div class="mdl-card__title mdl-card--border">
            <div class="mdl-card__title-text">Shipping</div>
        </div>
        <div class="mdl-card__supporting-text mdl-grid">
            <div class="mdl-cell mdl-cell--3-col label">
                Address
            </div>
            <div class="mdl-cell mdl-cell--8-col value">
                <address>
                    Mahfuz Khan<br>
                    1288 Marietta Street<br>
                    Santa Rosa, CA 95401</br>
                </address>
            </div>
            <div class="mdl-cell mdl-cell--3-col label">
                Method
            </div>
            <div class="mdl-cell mdl-cell--8-col value">
                Box and Shipping Label
            </div>
        </div>
    </div>
    <div class="payment-details mdl-card mdl-card--border mdl-cell mdl-cell--6-col">
        <div class="mdl-card__title mdl-card--border">
            <div class="mdl-card__title-text">Payment</div>
        </div>
        <div class="mdl-card__supporting-text mdl-grid">

            <div class="mdl-cell mdl-cell--3-col label">
                Method
            </div>
            <div class="mdl-cell mdl-cell--8-col value">
                Check Delivery
            </div>
            <div class="mdl-cell mdl-cell--3-col label">
                Address
            </div>
            <div class="mdl-cell mdl-cell--8-col value">
                <address>
                    Mahfuz Khan<br>
                    1288 Marietta Street<br>
                    Santa Rosa, CA 95401</br>
                </address>
            </div>
        </div>
    </div>
</div>