@extends("app")
@section("window-title")
    How To Turn Off Find My iPhone
@stop
@section("content")
    <div class="grid-container-wrapper">
        <div class="container">
            <div class="page-container colored col-md-8 col-md-offset-2 col-sm-10 col-xs-12 col-sm-offset-1 mdl-card mdl-shadow--2dp">
                <h2 class="page-title colorized bordered">How To Turn Off Find My iPhone?</h2>

                <div class="page-content section">
                    <div class="row">
                        <div style="height:400px;text-align:center;margin:10px 0px;" class="col-md-4">
                            <img src="images/icloud/1.png"/>
                            <h4>1. Select Settings</h4>
                        </div>
                        <div style="height:400px;text-align:center;margin:10px 0px;" class="col-md-4">
                            <img src="images/icloud/2.png"/>
                            <h4>2. Select Cloud</h4>
                        </div>
                        <div style="height:400px;text-align:center;margin:10px 0px;" class="col-md-4">
                            <img src="images/icloud/3.png"/>
                            <h4>3. Locate Find My iPhone</h4>
                        </div>
                        <div style="height:400px;text-align:center;margin:10px 0px;" class="col-md-4">
                            <img src="images/icloud/4.png"/>
                            <h4>4. Type In Your Password</h4>
                        </div>
                        <div style="height:400px;text-align:center;margin:10px 0px;" class="col-md-4">
                            <img src="images/icloud/5.png"/>
                            <h4>5. Find My iPhone should now be disabled</h4>
                        </div>
                    </div>
                    <br>
                </div>
            </div>
        </div>
    </div>
@stop