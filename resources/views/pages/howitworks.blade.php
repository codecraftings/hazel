@extends("app")
@section("window-title")
    How It Works
@stop
@section("content")
    <div class="grid-container-wrapper">
        <div class="container">
            <div class="page-container colored col-md-8 col-md-offset-2 col-sm-10 col-xs-12 col-sm-offset-1 mdl-card mdl-shadow--2dp">
                <h2 class="page-title colorized bordered">How It Works?</h2>

                <div class="page-content">
                    <div class="section colorized mdl-grid instruction-step">
                        <div class="mdl-cell mdl-cell--3-col v-centered">
                            <div class="h-centered">
                                <i class="material-icons">laptop</i>
                            </div>
                        </div>
                        <div class="mdl-cell mdl-cell--9-col">
                            <h3>1. Get a free offer:</h3>

                            <p>Begin by searching for your item in our database and answering a few questions to get an instant cash offer. We are continuously adding new products to our database, but if you can't find your item(s), please request a personalized offer and you'll receive one within 24 hours1. Offers are guaranteed-no gimmicks or games. We make cash offers on just about anything of substantial value, so don't hesitate to request offers on all your items. All offers are valid for a minimum of 14 days.</p>
                        </div>
                    </div>
                    <div class="section colorized mdl-grid instruction-step">
                        <div class="mdl-cell mdl-cell--3-col v-centered">
                            <div class="h-centered">
                                <i class="material-icons">card_giftcard</i>
                            </div>
                        </div>
                        <div class="mdl-cell mdl-cell--9-col">
                            <h3>2. Ship it to Us:</h3>

                            <p>We pays for your shipping by providing a free pre-paid shipping label. There's no waiting; you can print out your label online immediately after accepting the offer. Simply package your item(s) with the supplied packing slip and shipping label and either drop it off at any UPS Store or UPS drop-off location or schedule a pick-up.</p>
                        </div>
                    </div>
                    <div class="section colorized mdl-grid instruction-step">
                        <div class="mdl-cell mdl-cell--3-col v-centered">
                            <div class="h-centered">
                                <i class="material-icons">attach_money</i>
                            </div>
                        </div>
                        <div class="mdl-cell mdl-cell--9-col">
                            <h3>3. Get paid fast:</h3>

                            <p>Payments are sent out within 24 hours1 of receipt of your item on the next business day, and you choose how you want to be paid. Payments sent via U.S. First Class Mail are delivered within 2 to 7 days. Payments via PayPal are sent instantly to your PayPal account. Payments sent via UPS Next Day Air2 are delivered within 1 business day. Payments sent as an Amazon Gift Card are delivered instantly via e-mail.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop