@extends("app")
@section("window-title")
    FAQ
@stop
@section("content")
    <div class="grid-container-wrapper">
        <div class="container">
            <div class="page-container colored col-md-8 col-md-offset-2 col-sm-10 col-xs-12 col-sm-offset-1 mdl-card mdl-shadow--2dp">
                <h2 class="page-title bordered">Frequently Asked Questions</h2>

                <div style="padding: 10px 25px;" class="page-content section">
                    <div class="accordin" accordin="true">
                        <h4 class="accordin-title">Must I register for a cellYourBrokenPhones.com account to sell my
                            item?</h4>

                        <div class="accordin-content">
                            <p>
                                Yes. It is meant to help you track anything you’ve sent in to us and to save your time
                                when you
                                retron again by saving your information. Your iReTon account is completely free.</p>
                        </div>
                    </div>
                    <div class="accordin" accordin>
                        <h4 class="accordin-title">What does cellYourBrokenPhones.com do with my personal
                            information?</h4>

                        <div class="accordin-content">

                            <p>
                                cellYourBrokenPhones.com will never sell, rent, or share your personal information
                                unless you
                                have specifically
                                opted in to receive offers from our partners. Every device that comes in is wiped clean
                                of all
                                information whether you’ve already done it or not. Please see our privacy policy for
                                more
                                details.</p>
                        </div>
                    </div>
                    <div class="accordin" accordin>

                        <h4 class="accordin-title">I forgot my password. What should I do?</h4>

                        <p class="accordin-content">
                            If you have forgotten your password, simply click on the ‘Forgot your password’ link on the
                            sign-in page and follow the on-screen directions to reset your password. This will send an
                            email
                            to your email address associated with your account. From a link in the email, you’ll be
                            asked to
                            create a new password.</p>
                    </div>
                    <div class="accordin" accordin="true">

                        <h4 class="accordin-title">I did not receive a confirmation email when I registered.</h4>

                        <div class="accordin-content">
                            If you have not received the confirmation email, it may have been blocked by a spam filter.
                            It
                            is important that you are able to receive email from cellYourBrokenPhones.com because we
                            send
                            you updates every
                            step of the way as your items go through our process. If you find the email in a spam
                            folder,
                            please adjust your setting appropriately, so you can accept future emails from
                            cellYourBrokenPhones.com. If our
                            emails are not caught in your spam filter, please reach out to our Customer Care Team, so we
                            can
                            help research and resolve the problem.
                        </div>
                    </div>

                    <div class="accordin" accordin><h4 class="accordin-title">Is there a limit to the number of items I
                            can sell to cellYourBrokenPhones.com?</h4>

                        <p class="accordin-content">
                            Not at all! The more items you sell, the more cash you make. If you have a large quantity of
                            anything you want to trade in, you should check out cellYourBrokenPhones.com for
                            Business.</p></div>
                    <div class="accordin" accordin><h4 class="accordin-title">I don’t have all the accessories that my
                            devices came with. Can I still sell it?</h4>

                        <p class="accordin-content">
                            Yes! We gladly accept your devices even if you don’t have all the accessories that it came
                            with.
                            However, if you can send in the accessories that came with your item (i.e. the charger,
                            battery,
                            etc.), we can often pay you more for your trade-in.</p></div>
                    <div class="accordin" accordin><h4 class="accordin-title">Can I sell multiple items to
                            cellYourBrokenPhones.com at once?</h4>

                        <p class="accordin-content">
                            Yes, definitely you can. Once you’ve accepted an offer for your first item, search for and
                            add
                            more items until you’re done. Once you’ve entered all of your devices, proceed to checkout.
                            If
                            you have a large quantity of items to sell, it may be easier for you to use our
                            cellYourBrokenPhones.com for
                            Business tool, which enables simple trade in of large quantities of items.
                        </p></div>
                    <div class="accordin" accordin><h4 class="accordin-title">How does cellYourBrokenPhones.com
                            determine
                            the value of my item?</h4>

                        <p class="accordin-content">
                            Our technology dynamically prices every item in our catalog. The system utilizes market data
                            from a variety of sources to determine the true market value of your item at any given time.
                        </p>
                    </div>
                    <div class="accordin" accordin><h4 class="accordin-title">Why can’t I find my item?</h4>

                        <p class="accordin-content">
                            The first thing you should do is try searching for keywords like the brand name and the
                            product
                            name with spaces between them. If you try that and have no luck, it’s possible that the
                            item
                            may
                            not be in our catalog yet. However, we’re always updating cellYourBrokenPhones.com with
                            new
                            products and
                            categories. If you don’t see your item, let us know and we will research it and let you
                            know
                            if
                            we can make you an offer here.
                        </p></div>
                    <div class="accordin" accordin><h4 class="accordin-title">Does cellYourBrokenPhones.com accept
                            items
                            from outside of the United States?</h4>

                        <p class="accordin-content">
                            No. We currently do not accept items from outside of the United States.
                            Shipping
                        </p></div>
                    <div class="accordin" accordin><h4 class="accordin-title">Where do I ship my item?</h4>

                        <p class="accordin-content">
                            After you’ve printed out your USPS shipping label, you can drop your box off at any USPS
                            location nationwide.
                        </p></div>
                    <div class="accordin" accordin><h4 class="accordin-title">How should I package my item?</h4>

                        <div class="accordin-content">
                            <p>

                                You should carefully package your item to ensure that it is not damaged in transit. Use
                                old
                                newspapers, bubble wrap, or other environmentally friendly packaging material to cushion
                                your
                                item. cellYourBrokenPhones.com is not responsible for shipping damage that may occur in
                                transit.
                            </p>

                            <p>
                                Electronic devices require special attention during packaging in order to ensure their
                                safe
                                arrival at the destination. If you have it, package your item first in its original
                                packaging
                                and then secure it in a box with plenty of packing material. If you don’t have the
                                original
                                packaging, antistatic bubble wrap and plenty of extra cushioning should be used when
                                packaging
                                these items. cellYourBrokenPhones.com is not responsible for devices that you ship. If
                                you
                                are
                                uncomfortable with
                                packaging your item yourself, consider having these items professionally packed at a
                                USPS
                                location.
                            </p>
                        </div>
                    </div>
                    <div class="accordin" accordin><h4 class="accordin-title">Does cellYourBrokenPhones.com offer any
                            other
                            shipping methods besides the USPS?</h4>

                        <p class="accordin-content">
                            We offer FREE shipping within the continental United States on transactions of at least $1
                            through
                            our partnership with the USPS. You are welcome to use any other shipping carrier at your own
                            expense.
                        </p></div>
                    <div class="accordin" accordin><h4 class="accordin-title">What if I have mailed my item to you but
                            forgot to include an accessory?</h4>

                        <p class="accordin-content">
                            If you forgot any part of your order, please ship it to us with attention to Customer Care
                            Department at 15466 Los Gatos BLVD., #109-46, Los Gatos, CA 95032. Include the name of the
                            item
                            sent
                            in, your name, and your transaction number, which is included in the email we sent you
                            immediately
                            following your transaction. Unfortunately, we cannot pay for shipping for items that you
                            forgot
                            using the original label.
                        </p></div>
                    <div class="accordin" accordin><h4 class="accordin-title">How can I check the status of my
                            item?</h4>

                        <p class="accordin-content">
                            If you have an cellYourBrokenPhones.com account, you can access this information by logging
                            in.
                            Click the My
                            Account
                            link near the top right corner of your screen. If you don’t have an account, you can access
                            the
                            status of a transaction by submitting the transaction number (looks like IRT12345678910) and
                            your
                            email address through our tracking system. Also, every time the item of your status is
                            updated,
                            we’ll send an email notice to you.
                        </p></div>
                    <div class="accordin" accordin><h4 class="accordin-title">When does my offer expire?</h4>

                        <p class="accordin-content">
                            The value of electronics typically declines over time, so we can only honor your offer for
                            30
                            days.
                            If we receive your item after the 30 day period, your offer will be expired. If you would
                            still
                            like
                            to sell your item to cellYourBrokenPhones.com, simply return to cellYourBrokenPhones.com.com
                            and
                            create a new transaction with the
                            updated price.
                        </p>
                    </div>
                    <div class="accordin" accordin><h4 class="accordin-title">I was given one mailing label, but I have
                            two
                            boxes. How can I receive an additional
                            label?</h4>

                        <p class="accordin-content">
                            We're happy to help you get another shipping label. Please contact customer care through
                            live
                            chat
                            or email to let us know, and we’ll send you another one.
                        </p></div>
                    <div class="accordin" accordin><h4 class="accordin-title">My label has a weight on it but my box is
                            heavier than that. What should I do?</h4>

                        <p class="accordin-content">
                            That’s okay. The shipping carrier will bill cellYourBrokenPhones.com for the difference in
                            price
                            if there is any.
                            Can I send more than one order in the same box?
                            Yes, you can. Please just make sure that you include all packing slips we sent you via
                            email.
                        </p></div>
                    <div class="accordin" accordin><h4 class="accordin-title">Is my package insured against loss or
                            damage?</h4>

                        <p class="accordin-content">
                            Your package is insured up to the value we offered you during your online transaction. This
                            insurance covers the shipment from loss due to carrier misconduct or loss of the entire
                            shipment.
                            However, it is not coverage for inadequate packaging. Please make sure you package them with
                            care.
                        </p></div>


                    <div class="accordin" accordin><h4 class="accordin-title">What happens once I have sent in my
                            item?</h4>

                        <p class="accordin-content">
                            Once your item is received, it will be hand evaluated by a trained cellYourBrokenPhones.com
                            team
                            member where
                            your
                            offer will be confirmed and all personal data removed. The vast majority of items move
                            through
                            this
                            process quickly, and we are able to send payment within about 7 business days of receiving
                            your
                            item. Throughout the process, we’ll send you email updates about the status of your item.
                            What happens if you disagree with the condition I select for my item?
                            If a discrepancy comes up during the inspection process, you will receive an email with the
                            new
                            value and the reason for any changes. You can either accept the offer or ask that we return
                            the
                            item
                            to you. If you do not respond to us within 5 days, we’ll assume you are all right with the
                            new
                            offer
                            and move forward automatically.
                        </p></div>
                    <div class="accordin" accordin><h4 class="accordin-title">What happens if I disagree with the value
                            after the inspection?</h4>

                        <p class="accordin-content">
                            If you disagree with our revised offer, we will return your item back to you at our expense.
                            In
                            the
                            email telling you about the change, there is a link which you can use to decline the new
                            offer.
                            Once
                            you let us know that is what you want, we’ll get it back to you in a couple of days. If you
                            do
                            not
                            respond to the new offer within 5 days, we will assume you are all right with the new offer
                            and
                            move
                            forward automatically.
                        </p></div>
                    <div class="accordin" accordin><h4 class="accordin-title">What does cellYourBrokenPhones.com do with
                            the
                            items it purchases?</h4>

                        <p class="accordin-content">
                            cellYourBrokenPhones.com believes that reuse is the highest form of sustainability. That is
                            why
                            we promote reuse
                            first and recycling as a last resort. We will get used devices back into the market to keep
                            working
                            products in use for as long as possible, and in turn, extending their life, preventing
                            e-waste,
                            and
                            ensuring you receive rewards for your used devices.
                        </p></div>
                    <div class="accordin" accordin><h4 class="accordin-title">How does cellYourBrokenPhones.com pay
                            me?</h4>
                        <p class="accordin-content">
                            cellYourBrokenPhones.com will send payment via PayPal or check by mail. You designate which
                            method of payment you
                            prefer as part of the checkout process.
                        </p></div>
                </div>
            </div>
        </div>
    </div>
@stop