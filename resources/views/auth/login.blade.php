@extends('app')

@section('content')

    <div class="container auth-container">
        <form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/login') }}">
            <div class="col-md-4 col-md-offset-4 form-card mdl-card mdl-shadow--2dp">
                <div class="mdl-card__title">
                    <h2 class="mdl-card__title-text">Member Login</h2>
                </div>
                <div class="mdl-card__supporting-text">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    {{--<input type="text" class="col-md-10 col-md-offset-1 form-item" name="email" onblur="if (this.value == '') {this.value = 'EMAIL';}" onfocus="this.value = '';" value="EMAIL">--}}
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label form-field">
                        <input class="mdl-textfield__input" type="text" id="loginf" name="email">
                        <label class="mdl-textfield__label" for="loginf">Email</label>
                    </div>
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label form-field">
                        <input class="mdl-textfield__input" type="password" id="loginpass" name="password">
                        <label class="mdl-textfield__label" for="loginpass">Password</label>
                    </div>
                    <label for="checkbox-1" class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect">
                        <input id="checkbox-1" type="checkbox" name="remember" class="mdl-checkbox__input">
                        </input>
                        <span class="mdl-checkbox__label">Remember Me</span>
                    </label>
                </div>
                <div class="mdl-card__actions mdl-card--border">
                    <button type="submit"
                            class="mdl-button mdl-js-button mdl-button--colored mdl-js-ripple-effect fr">
                        Submit
                    </button>
                </div>
                <div class="clearfix"></div>
            </div>
        </form>
    </div>

@endsection
