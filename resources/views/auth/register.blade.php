@extends('app')

@section('content')

    <div class="container auth-container">
        <form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/register') }}">
            <div class="col-md-4 col-md-offset-4 form-card mdl-card mdl-shadow--2dp">
                <div class="mdl-card__title">
                    <h2 class="mdl-card__title-text">Register</h2>
                </div>
                <div class="mdl-card__supporting-text">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label form-field">
                        <input class="mdl-textfield__input" type="text" id="namef" name="name">
                        <label class="mdl-textfield__label" for="namef">Name</label>
                    </div>
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label form-field">
                        <input class="mdl-textfield__input" type="text" id="loginf" name="email">
                        <label class="mdl-textfield__label" for="loginf">Email</label>
                    </div>
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label form-field">
                        <input class="mdl-textfield__input" type="password" id="loginpass" name="password">
                        <label class="mdl-textfield__label" for="loginpass">Password</label>
                    </div>
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label form-field">
                        <input class="mdl-textfield__input" type="password" id="loginconfpass"
                               name="password_confirmation">
                        <label class="mdl-textfield__label" for="loginconfpass">Confirm Password</label>
                    </div>
                    <label for="checkbox-1" class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect">
                        <input id="checkbox-1" type="checkbox" name="remember" class="mdl-checkbox__input">
                        </input>
                        <span class="mdl-checkbox__label">accept our terms and condition</span>
                    </label>
                </div>
                <div class="mdl-card__actions mdl-card--border">
                    <button type="submit"
                            class="mdl-button mdl-js-button mdl-button--colored mdl-js-ripple-effect fr">
                        Submit
                    </button>
                </div>
                <div class="clearfix"></div>
            </div>
        </form>
    </div>

@endsection
