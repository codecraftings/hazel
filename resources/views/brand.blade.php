@extends('app')
@section("window-title")
    Select Your Brand
@stop
@section('content')

    <div class="grid-container-wrapper">
        <div class="container">
            <div style="padding:15px;" class="col-sm-10 col-xs-12 col-md-offset-1 flat-grid-container mdl-card mdl-shadow--2dp">
                <div class="row">
                    <div class="mdl-Ecell mdl-Ecell--3-col col-sm-3 hidden-xs">
                        <div style="height:auto;margin-top:61px;text-align:center;position:relative;" class="">
                            <img style="max-width:90%;" src="{{ url('images/phones/sample3.png') }}">
                            <img style="width: 70%;margin:auto;position:absolute;top:0;left:0;right:0;bottom:0;"
                                 src="{{url('images/logo.png')}}"/>
                        </div>
                    </div>
                    <div class="mdl-Ecell mdl-Ecell--9-col col-sm-9">
                        <div class="mdl-card__title mdl-card--border">
                            <div class="mdl-card__title-text grid-title">Select Manufacturer of Your Device</div>
                        </div>
                        <div class="flat-grid col-xs-12">
                            @foreach($brands as $brand)
                                <div class="col-sm-4 col-xs-6 flat-grid-item">
                                    <div onclick="location.href='{{url('brands/'.$brand->id)}}'"
                                         class="col-sm-12 flat-grid-item-inner">
                                        <div class="flat-grid-item-img-container">
                                            <img src="{{ $brand->logo_url }}" class="flat-grid-item-img">
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection