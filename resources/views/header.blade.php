<nav class="navbar navbar-default custom-navbar-height">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand text-logo" href="{{url('/')}}">sellBrokenPhones</a>
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>

        <div class="collapse navbar-collapse hidden-xs" id="bs-example-navbar-collapse-1">
            <ul navbar-model class="nav navbar-nav btn-lg navbar-right header-menu">
                <li><a href="{{ url('/brands') }}">Sell Your Device</a></li>
                <li><a href="{{ url('pages/howitworks') }}">How It Works?</a></li>
                <li><a href="{{ url('pages/faq') }}">FAQ</a></li>
                <li ng-hide="user.isLoggedIn()" class="{{Auth::guest()?"":"ng-hide"}}"><a href="{{ url('/auth/login') }}">Login</a></li>
                {{--<li class=""><button style="padding: 0px 20px;" class="mdl-button mdl-js-button flat-btn2 mdl-js-ripple-effect" onclick="top.location.href='{{ url('/auth/login') }}'">Login</button></li>--}}
                <li ng-show="user.isLoggedIn()" class="{{Auth::guest()?"ng-hide":""}}">
                    <button class="mdl-button mdl-js-button user-menu-toggle" id="user_menu"><span
                                class="material-icons">keyboard_arrow_down</span>My Account
                    </button>
                    <ul class="user-menu mdl-menu mdl-menu--bottom-left mdl-js-menu mdl-js-ripple-effect"
                        for="user_menu">
                        <li onclick="top.location.href='{{url('/offers')}}'" class="mdl-menu__item"><a
                                    href="{{ url('/offers') }}">Your Offers</a></li>
                        <li onclick="top.location.href='{{url('/auth/logout')}}'"  class="mdl-menu__item"><a href="{{ url('/auth/logout') }}">Logout</a></li>
                    </ul>
                </li>
                <li class="" cart-button></li>
            </ul>
        </div>
    </div>
</nav>