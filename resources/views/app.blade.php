<!DOCTYPE html>
<html lang="en">
<head>
	<base href="{{url('/')}}/">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>@yield("window-title", "Home") | Sell Your Broken Phones</title>

	<link href='http://fonts.googleapis.com/css?family=Roboto:400,500,300|Roboto+Condensed:300, 300italic,400,700' rel='stylesheet' type='text/css'>
	<link href="{{ asset('/css/app.css') }}" rel="stylesheet">
	<!-- Fonts -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<script>
		var base_url = "{{url('/')}}";
		var api_url = "{{url('/')}}";
		var asset_url = "{{url('/')}}";
		@if(\Auth::check())
		var userDataFromServer = {!! \Auth::user() !!};
		@endif
	</script>
</head>
<body ng-app="app" class="@yield('page_class')">
	@include('header')

	@yield('content')

	@include('footer')

	<!-- Scripts -->
	{{--<script src="{{asset('js/common.js')}}"></script>--}}
	<script src="{{ asset( 'js/app.js' ) }}"></script>
</body>
</html>
