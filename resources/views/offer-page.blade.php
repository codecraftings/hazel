@extends('app')
@section("window-title")
    Your Offers
@stop
@section('content')
    <div class="grid-container-wrapper">
        <div class="container">
            <div ng-controller="OfferPageController as vm"
                 class="offer-details-card confirmation-page-container flat-grid-container mdl-card mdl-shadow--2dp">
                <div offer-details="vm.offerData"></div>
            </div>
        </div>
    </div>
    <script>
        var offerData = <?php echo $offer;?>;
    </script>
@stop