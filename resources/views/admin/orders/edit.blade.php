@extends('admin.layouts.master')
@section('body')
    @include('admin.common.top-menu',array('page_id'=>'phones'))
    <div ng-app="app" class="account-container big">

        <div class="content clearfix">
            {!!Form::model($order, array('enctype'=>"multipart/form-data", 'autocomplete'=>"off", 'method'=>'post'))!!}

            <div class="form-fields">
                <h1>Edit The Offer</h1>
                @if(Session::has('error'))
                    <div class="alert alert-error">
                        {{Session::get('error')}}
                    </div>
                @endif
                @if(Session::has('success'))
                    <div class="alert alert-success">
                        {{Session::get('success')}}
                    </div>
                @endif
                {!!Form::token()!!}
                <div class="field clearfix">
                    {!!Form::label('total_value','Total Value')!!}
                    {!!Form::text('total_value', null, array('required'=>'true', 'placeholder'=>'Total Value', 'class'=>'input'))!!}
                </div>
                <div class="field clearfix">
                    {!!Form::label('status','Select Status')!!}
                    {!!Form::select('status', [
                        "awaiting_review" => "Awaiting Review",
                        "label_sent" => "Shipping Label Sent",
                        "awaiting_shipment" => "Awaiting Shipment",
                        "shipment_received" => "Shipment Received",
                        "payment_sent" => "Payment Sent",
                        "cleared" => "Cleared",
                    ], null, array('required'=>'true', 'class'=>'input'))!!}
                </div>
            </div> <!-- /login-fields -->

            <div class="actions clearfix">

                <button class="button btn btn-success btn-large">Submit</button>

            </div> <!-- .actions -->



            {!!Form::close()!!}

        </div> <!-- /content -->

    </div>
@stop