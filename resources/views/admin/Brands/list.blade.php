@extends('admin.layouts.master')
@section('body')
    @include('admin.common.top-menu',array('page_id'=>'brands'))
    <div class="main">
        <div class="main-inner">
            <div class="container">
                <div class="row">
                    <div class="span12">
                        <div class="widget widget-table action-table">
                            <form autocomplete="off" style="margin-top:15px;" id="filterform" class="form-inline hidden" method="get" action="">
                                @if(Session::has('error'))
                                    <div class="alert alert-error">
                                        {{Session::get('error')}}
                                    </div>
                                @endif
                                @if(Session::has('success'))
                                    <div class="alert alert-success">
                                        {{Session::get('success')}}
                                    </div>
                                @endif
                                <label for="sort">Sort By</label>
                                <select onchange="document.getElementById('filterform').submit();" id="sort" name="sort">
                                    <option{{Input::get('sort')=="updated"?" selected":""}} value="updated">Last Updated</option>
                                    <option{{Input::get('sort')=="new"?" selected":""}} value="new">Newly Added</option>
                                    <option{{Input::get('sort')=="old"?" selected":""}} value="old">Older</option>
                                </select>
                                <label for="pagging">Pagging </label>
                                <select onchange="document.getElementById('filterform').submit();" id="pagging" name="pagging">
                                    <option{{Input::get('pagging')=="100"?" selected":""}} value="100">100</option>
                                    <option{{Input::get('pagging')=="200"?" selected":""}} value="200">200</option>
                                    <option{{Input::get('pagging')=="500"?" selected":""}} value="500">500</option>
                                    <option{{Input::get('pagging')=="1000"?" selected":""}} value="1000">1000</option>
                                </select>
                            </form>
                            <div class="widget-header"> <i class="icon-th-list"></i>
                                <h3>List of Brands</h3>
                            </div>
                            <!-- /widget-header -->
                            <div class="widget-content">
                                <table class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Image</th>
                                        <th class="td-actions"> </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($items as $item)
                                        <tr>
                                            <td>{{$item->name}}</td>
                                            <td><img style="width:100px;height:80px;" src="{{$item->logo_url}}"></td>
                                            <td class="td-actions">
                                                <a target="_blank" href="{{$item->permalink}}" class="btn btn-small btn-success">
                                                    <i class="btn-icon-only icon-link"> </i>
                                                </a>
                                                <a href="{{admin_url('brands/edit/'.$item->id)}}" class="btn btn-small btn-primary">
                                                    <i class="btn-icon-only icon-pencil"> </i>
                                                </a>
                                                <a href="javascript:;" onclick="if(confirm('Do you really want to delete this Brand?')){document.getElementById('delete-form-{{$item->id}}').submit();}" class="btn btn-danger btn-small">
                                                    <i class="btn-icon-only icon-trash"> </i>
                                                </a>
                                                <form method="post" id="delete-form-{{$item->id}}" name="delete-form" action="{{admin_url('brands/delete/'.$item->id)}}">
                                                    {!!Form::token()!!}
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    </tfoot>
                                </table>
                            </div>
                            {!!$items->appends(Input::only('pagging','sort'))->render()!!}
                            <!-- /widget-content -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /widget -->
@stop