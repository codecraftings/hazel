@extends('admin.layouts.master')
@section('body')
    @include('admin.common.top-menu',array('page_id'=>'brands'))
    <div class="account-container big">

        <div class="content clearfix">
            {!!Form::open(array('enctype'=>"multipart/form-data", 'autocomplete'=>"off", 'method'=>'post'))!!}

            <div class="form-fields">
                <h1>Add Brand</h1>
                @if(Session::has('error'))
                    <div class="alert alert-error">
                        {{Session::get('error')}}
                    </div>
                @endif
                @if(Session::has('success'))
                    <div class="alert alert-success">
                        {{Session::get('success')}}
                    </div>
                @endif
                {!!Form::token()!!}
                <div class="field clearfix">
                    {!!Form::label('name','Brand Name')!!}
                    {!!Form::text('name', null, array('required'=>'true', 'placeholder'=>'Brand Name', 'class'=>'input'))!!}
                </div>
                <div class="field clearfix">
                    {!!Form::label('logo','Brand Logo')!!}
                    {!!Form::file('logo', array('class'=>'input'))!!}
                </div>
            </div> <!-- /login-fields -->

            <div class="actions clearfix">

                <button class="button btn btn-success btn-large">Submit</button>

            </div> <!-- .actions -->



            {!!Form::close()!!}

        </div> <!-- /content -->

    </div>
@stop