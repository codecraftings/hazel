<div class="subnavbar">
  <div class="subnavbar-inner">
    <div class="container">
      <ul class="mainnav">
        <li class="{{$page_id=='dashboard'?'active':''}}"><a href="{{admin_url('dashboard')}}"><i class="icon-dashboard"></i><span>Dashboard</span> </a> </li>
        @if(isset($admin))
        <li class="dropdown{{$page_id=='users'?' active':''}}"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-group"></i><span>Users</span><b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="{{admin_url('users/list')}}">View All Users</a></li>
          </ul>
        </li>
        @endif
        <li class="dropdown{{$page_id=='brands'?' active':''}}"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-cloud"></i><span>Brands</span><b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="{{admin_url('brands/list')}}">View All Brands</a></li>
            <li><a href="{{admin_url('brands/add')}}">Add A Brand</a></li>
          </ul>
        </li>
        <li class="dropdown{{$page_id=='events'?' active':''}}"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-mobile-phone"></i><span>Phones</span><b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="{{admin_url('phones/list')}}">View All Phones</a></li>
            <li><a href="{{admin_url('phones/add')}}">Add A Phone</a></li>
          </ul>
        </li>
        <li class="dropdown{{$page_id=='reviews'?' active':''}}"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-book"></i><span>Orders</span><b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="{{admin_url('orders/list')}}">View All Orders</a></li>
          </ul>
        </li>
        <li class="dropdown{{$page_id=='settings'?' active':''}}"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-cogs"></i><span>Settings</span><b class="caret"></b></a> 
          <ul class="dropdown-menu">
            <li><a href="{{admin_url('edit-account')}}">Change Password</a></li>
          </ul>
        </li>
      </ul>
    </div>
    <!-- /container --> 
  </div>
  <!-- /subnavbar-inner --> 
</div>
<!-- /subnavbar -->