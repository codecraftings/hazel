@extends('admin.layouts.master')
@section('body')
@include('admin.common.top-menu', array('page_id'=>'dashboard'))
<div class="main">
	<div class="main-inner">
		<div class="container">
			<div class="row">
				<div class="span6">
					<div class="widget">
						<div class="widget-header"> <i class="icon-bookmark"></i>
							<h3>Quick Navigations</h3>
						</div>
						<!-- /widget-header -->
						<div class="widget-content">
							<div class="shortcuts">
								<a href="{{admin_url('orders/list')}}" class="shortcut">
									<i class="shortcut-icon icon-book"></i>
									<span class="shortcut-label">View Latest Orders</span>
								</a>
								<a href="{{admin_url('phones/list')}}" class="shortcut">
									<i class="shortcut-icon icon-mobile-phone"></i>
									<span class="shortcut-label">View All Phones</span>
								</a>
								<a href="{{admin_url('phones/add')}}" class="shortcut">
									<i class="shortcut-icon icon-save"></i>
									<span class="shortcut-label">Add New Phone</span>
								</a>
								<a href="{{admin_url('edit-account')}}" class="shortcut">
									<i class="shortcut-icon icon-lock"></i>
									<span class="shortcut-label">Change Password</span> 
								</a>
								<a href="{{admin_url('edit-account')}}" class="shortcut">
									<i class="shortcut-icon icon-envelope-alt"></i>
									<span class="shortcut-label">Change Email</span> 
								</a>
							</div>
							<!-- /shortcuts --> 
						</div>
						<!-- /widget-content --> 
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop