<div class="field clearfix">
    <label>Questions</label>
</div>
<div questions q-text="{{isset($questions)?json_encode($questions): ""}}">
    <div class="field clearfix" id="questions">
        <div style="position:relative;border-bottom: 1px solid #ccc;padding:8px 0px;" ng-repeat="q in questions"
             class="qt-question clearfix">
            <a href="javascript:void(0)" class="cross-button" ng-click="delete_row($index)">X</a>

            <div style="margin-top:10px;" class="form-horizontal">
                <label style="margin:5px 20px;width:100px;" class="col-md-5">Title</label>
                <input class="col-md-5" type="text" ng-model="q.title" placeholder="Question Title">
            </div>
            <div style="margin-top:10px;" class="form-horizontal">
                <label style="margin:5px 20px;width:100px;" class="col-md-5">Explanation</label>
                <textarea class="col-md-5" ng-model="q.explanation" placeholder="Question Explanation"></textarea>
            </div>
            <div style="margin-top:10px;" class="form-horizontal">
                <label style="margin:5px 20px;width:100px;" class="col-md-5">Image(<a target="_blank" ng-if="q.image" ng-href="{{url("/")}}/@{{q.image}}">Preview</a>)</label>
                <input insta-upload="img_@{{$index}}" class="col-md-5" type="text" ng-model="q.image" placeholder="Click To Add Image">

            </div>
            <div style="margin: 10px 0px;" class="col-md-12">
                <label style="margin:5px 20px">Options</label>

                <div style="float:left">
                    <div style="position:relative;margin: 10px 0px;" class="form-inline clearfix"
                         ng-repeat="opt in q.options">
                        <a style="right:-20px;top:0px;" href="javascript:void(0)" class="cross-button"
                           ng-click="delete_opt(q, $index)">X</a>

                        <div style="margin-top:10px;" class="form-group">

                            <label for="i1" style="margin:5px 5px">Label</label>
                            <input ng-change="normalize_opt_label(opt)" id="i1" style="width:40px;" type="text"
                                   ng-model="opt.label" placeholder="Value">
                        </div>
                        <div style="margin-top:10px;" class="form-group">

                            <label for="i4" style="margin:5px 5px">Image(<a target="_blank" ng-if="opt.image" ng-href="{{url("/")}}/@{{opt.image}}">Preview</a>)</label>
                            <input insta-upload="opt_img_@{{$index}}" id="i4" style="width:40px;" type="text"
                                   ng-model="opt.image" placeholder="Optional">
                        </div>
                        <div style="margin-top:10px;" class="form-group">

                            <label for="i4" style="margin:5px 5px">Icon(<a target="_blank" ng-if="opt.icon" ng-href="{{url("/")}}/@{{opt.icon}}">Preview</a>)</label>
                            <input insta-upload="ico_@{{$index}}" id="i4" style="width:40px;" type="text"
                                   ng-model="opt.icon" placeholder="Optional">
                        </div>
                        <div style="margin-top:10px;" class="form-group">

                            <label for="i2" style="margin:5px 5px">Add</label>
                            <input id="i2" style="width:40px;" type="text" ng-model="opt.add" placeholder="Add Value">
                        </div>
                        <div style="margin-top:10px;" class="form-group">
                            <label for="i3" style="margin:5px 5px">Minus</label>
                            <input id="i3" style="width:40px;" ng-model="opt.minus"
                                   placeholder="Minus Value">
                        </div>

                    </div>
                    <a href="javascript:void(0)" ng-click="add_option(q)">Add Option</a>
                </div>
            </div>
        </div>
    </div>
    <div class="field clearfix">
        <a href="javascript:void(0)" ng-click="add_row()">Add Question</a>
    </div>
    <input type="text" class="hidden" name="questions" ng-model="qText">
</div>