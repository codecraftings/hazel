@extends('admin.layouts.master')
@section('body')
    @include('admin.common.top-menu',array('page_id'=>'phones'))
    <div ng-app="app" class="account-container big">

        <div class="content clearfix">
            {!!Form::open(array('enctype'=>"multipart/form-data", 'autocomplete'=>"off", 'method'=>'post'))!!}

            <div class="form-fields">
                <h1>Add a Phone</h1>
                @if(Session::has('error'))
                    <div class="alert alert-error">
                        {{Session::get('error')}}
                    </div>
                @endif
                @if(Session::has('success'))
                    <div class="alert alert-success">
                        {{Session::get('success')}}
                    </div>
                @endif
                {!!Form::token()!!}
                <div class="field clearfix">
                    {!!Form::label('name','Phone Name')!!}
                    {!!Form::text('name', null, array('required'=>'true', 'placeholder'=>'Phone Name', 'class'=>'input'))!!}
                </div>
                <div class="field clearfix">
                    {!!Form::label('base_price','Base Price')!!}
                    {!!Form::text('base_price', null, array('required'=>'true', 'placeholder'=>'Base Price', 'class'=>'input'))!!}
                </div>
                <div class="field clearfix">
                    {!!Form::label('brand_id','Select Phone Brand')!!}
                    {!!Form::select('brand_id', $brands, null, array('required'=>'true', 'class'=>'input'))!!}
                </div>
                <div class="field clearfix">
                    {!!Form::label('image','Phone Image')!!}
                    {!!Form::file('image', array('class'=>'input', 'required'=>true))!!}
                </div>
                @include("admin.phones.questions")
            </div> <!-- /login-fields -->

            <div class="actions clearfix">

                <button class="button btn btn-success btn-large">Submit</button>

            </div> <!-- .actions -->



            {!!Form::close()!!}

        </div> <!-- /content -->

    </div>
@stop