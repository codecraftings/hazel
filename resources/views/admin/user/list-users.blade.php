@extends('admin.layouts.master')
@section('body')
@include('admin.common.top-menu',array('page_id'=>'users'))
<div class="main">
	<div class="main-inner">
		<div class="container">
			<div class="row">
				<div class="span10">
					<div class="widget widget-table action-table">
						<form autocomplete="off" style="margin-top:15px;" id="filterform" class="form-inline" method="get" action="">
							@if(Session::has('error'))
							<div class="alert alert-error">
								{{Session::get('error')}}
							</div>
							@endif
							@if(Session::has('success'))
							<div class="alert alert-success">
								{{Session::get('success')}}
							</div>
							@endif
							<label for="sort">Sort By</label>
							<select onchange="document.getElementById('filterform').submit();" id="sort" name="sort">
								<option{{Input::get('sort')=="updated"?" selected":""}} value="updated">Last Updated</option>
								<option{{Input::get('sort')=="new"?" selected":"";}} value="new">New users</option>
								<option{{Input::get('sort')=="old"?" selected":""}} value="old">Old users</option>
							</select>
							<label for="pagging">Pagging </label>
							<select onchange="document.getElementById('filterform').submit();" id="pagging" name="pagging">
								<option{{Input::get('pagging')=="100"?" selected":""}} value="100">100</option>
								<option{{Input::get('pagging')=="200"?" selected":""}} value="200">200</option>
								<option{{Input::get('pagging')=="500"?" selected":""}} value="500">500</option>
								<option{{Input::get('pagging')=="1000"?" selected":""}} value="1000">1000</option>
							</select>
						</form>
						<div class="widget-header"> <i class="icon-th-list"></i>
							<h3>List of users</h3>
						</div>
						<!-- /widget-header -->
						<div class="widget-content">
							<table class="table table-striped table-bordered">
								<thead>
									<tr>
										<th>Name</th>
										<th>Email</th>
										<th>Email Status</th>
										<th>Nickname</th>
										<th>Gender</th>
										<th>Registered</th>
										<th>Role</th>
										<th class="td-actions"> </th>
									</tr>
								</thead>
								<tbody>
									@foreach($users as $user)
									<tr>
										<td>{{$user->present()->name()}}</td>
										<td>{{$user->email}}</td>
										<td>{{$user->email_status=="verified"?"Verified":"Not Verified"}}</td>
										<td>{{$user->nickname}}</td>
										<td>{{ucfirst($user->gender)}}</td>
										<td>{{$user->created_at->diffForHumans()}} ago</td>
										<td>{{$user->role_id}}</td>
										<td class="td-actions">
											<a target="_blank" href="{{$user->profile_url}}" class="btn btn-small btn-success">
												<i class="btn-icon-only icon-link"> </i>
											</a>
											<a href="{{admin_url('users/edit/'.$user->id)}}" class="btn btn-small btn-primary">
												<i class="btn-icon-only icon-pencil"> </i>
											</a>
											<a href="javascript:;" onclick="if(confirm('Do you really want to delete this user?')){document.getElementById('delete-form-{{$user->id}}').submit();}" class="btn btn-danger btn-small">
												<i class="btn-icon-only icon-trash"> </i>
											</a>
											<form method="post" id="delete-form-{{$user->id}}" name="delete-form" action="{{admin_url('users/delete/'.$user->id)}}">
											{{Form::token()}}
											</form>
										</td>
									</tr>
									@endforeach
								</tbody>
								<tfoot>
								</tfoot>
							</table>
						</div>
						{{$users->appends(Input::only('pagging','sort'))->render()}}
						<!-- /widget-content --> 
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /widget --> 
@stop