@extends('admin.layouts.master')
@section('body')
@include('admin.common.top-menu',array('page_id'=>'event'))
<div class="account-container big">
	
	<div class="content clearfix">
		{{Form::model($admin, array('enctype'=>"", 'autocomplete'=>"off", 'method'=>'post'))}}

		<div class="form-fields">
			<h1>Edit Account Info</h1>	
			@if(Session::has('error'))
			<div class="alert alert-error">
				{{Session::get('error')}}
			</div>
			@endif
			@if(Session::has('success'))
			<div class="alert alert-success">
				{{Session::get('success')}}
			</div>
			@endif
			{{Form::token()}}
			<div class="field clearfix">
				{{Form::label('first_name','First Name')}}
				{{Form::text('first_name', null, array('required'=>'true', 'placeholder'=>'First Name', 'class'=>'input'))}}
			</div>
			<div class="field clearfix">
				{{Form::label('last_name','Last Name')}}
				{{Form::text('last_name', null, array('required'=>'', 'placeholder'=>'Last Name', 'class'=>'input'))}}
			</div>
			<div class="field clearfix">
				{{Form::label('nickname','Nickname')}}
				{{Form::text('nickname', null, array('required'=>'true', 'placeholder'=>'Nickname', 'class'=>'input'))}}
			</div>
			<div class="field clearfix">
				{{Form::label('email','Email')}}
				{{Form::text('email', null, array('required'=>'true', 'placeholder'=>'Email', 'class'=>'input'))}}
			</div>
			<div class="field clearfix">
				{{Form::label('oldpassword','Old Password')}}
				{{Form::password('oldpassword', array('placeholder'=>'Old Password', 'class'=>'input'))}}
			</div>
			<div class="field clearfix">
				{{Form::label('password','New Password')}}
				{{Form::password('password', array('placeholder'=>'New Password', 'class'=>'input'))}}
			</div>
			<div class="field clearfix">
				{{Form::label('password_confirmation','Confirm Password')}}
				{{Form::password('password_confirmation', array('placeholder'=>'Confirm Password', 'class'=>'input'))}}
			</div>
		</div> <!-- /login-fields -->

		<div class="actions clearfix">

			<button class="button btn btn-success btn-large">Submit</button>

		</div> <!-- .actions -->



		{{Form::close()}}

	</div> <!-- /content -->

</div>
@stop