@extends('app')
@section("window-title")
    Checkout
@stop
@section('content')
    <div ng-controller="CheckoutController as vm" class="grid-container-wrapper">
        <div class="container">
            <div class="steps-container flat-grid-container mdl-card mdl-grid mdl-shadow--2dp">
                <div class="mdl-cell mdl-cell--8-col">

                    <div class="steps-nav-container mdl-card__title mdl-card--border">
                        <ul class="steps-nav">
                            <li class="button">
                                <button ng-disabled="vm.step1.disabled" ng-class="{'active':vm.step1.visible===true}"
                                        ng-click="vm.navigate(1)"
                                        class="step-button mdl-button mdl-js-button mdl-js2-ripple-effect">
                                    Step 1
                                </button>
                            </li>
                            <li class="arrow"><i class="material-icons">play_arrow</i></li>
                            <li class="button">
                                <button ng-disabled="vm.step2.disabled" ng-class="{'active':vm.step2.visible===true}"
                                        ng-click="vm.navigate(2)"
                                        class="step-button mdl-button mdl-js-button mdl-js2-ripple-effect">
                                    Step 2
                                </button>
                            </li>
                            <li class="arrow"><i class="material-icons">play_arrow</i></li>
                            <li class="button">
                                <button ng-disabled="vm.step3.disabled" ng-class="{'active':vm.step3.visible===true}"
                                        ng-click="vm.navigate(3)"
                                        class="step-button mdl-button mdl-js-button mdl-js2-ripple-effect active2">
                                    Step 3
                                </button>
                            </li>
                        </ul>
                    </div>
                    <div ng-show="vm.uiBusy"
                         class="step-loader">
                        <div class="mdl-spinner mdl-spinner--single-color mdl-js-spinner is-active"></div>
                    </div>
                    <div ng-show="vm.step1.visible" id="step1" class="step-container">
                        <form name="form1" novalidate>
                            <div class="step-header">
                                <h3>Enter your email to continue</h3>

                                <p>If your email is already register, you will be promoted to login and if not you
                                    can create a password.</p>
                            </div>
                            <div class="alert alert-danger" role="alert"
                                 ng-show="vm.step1.error">@{{ vm.step1.error }}</div>
                            <div class="step-fields">
                                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label form-field">
                                    <div ng-show="vm.step1.loaderVisible"
                                         class="mdl-spinner mdl-spinner--single-color mdl-js-spinner is-active inside-tf-loader"></div>
                                    <input class="mdl-textfield__input" type="email" pattern="^[\S]+@[\S]+\.[A-z0-9]+$"
                                           ng-change="vm.step1.onEmailChange($event)" ng-model="vm.step1.email"
                                           id="emailf" name="email" required>

                                    <label class="mdl-textfield__label" for="emailf">Email</label>
                                </div>
                                <div ng-if="vm.step1.loginMode">
                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label form-field">
                                        <input class="mdl-textfield__input" type="password" ng-model="vm.step1.password"
                                               id="pass_field" required>
                                        <label class="mdl-textfield__label" for="pass_field">Password</label>
                                    </div>
                                </div>
                                <div ng-if="vm.step1.signupMode">
                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label form-field">
                                        <input class="mdl-textfield__input" type="password"
                                               ng-model="vm.step1.new_password" name="new_pass" id="new_passf" required>
                                        <label class="mdl-textfield__label" for="new_passf">New Password</label>
                                    </div>
                                    <div ng-class="{'ng-invalid':form1.confirm_newpass.$error.$match&&form1.confirm_newpass.$dirty}"
                                         class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label form-field">
                                        <input class="mdl-textfield__input" type="password"
                                               ng-model="vm.step1.confirm_password" name="confirm_newpass"
                                               match="new_pass" id="confirm_newpass" required>
                                        <label class="mdl-textfield__label" for="confirm_newpass">Confirm New
                                            Password</label>
                                        <span class="mdl-textfield__error">Password mismatch!</span>
                                    </div>
                                </div>
                            </div>
                            <div class="step-actions">
                                <button ng-click="vm.step1.submit()"
                                        ng-disabled="form1.$invalid||(!vm.step1.loginMode&&!vm.step1.signupMode)||vm.uiBusy"
                                        class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect">
                                    Continue >>
                                </button>
                            </div>
                        </form>
                    </div>
                    <div ng-show="vm.step2.visible" id="step2" class="step-container">
                        <div class="step-header">
                            <h3>Choose how you'd like to get paid</h3>

                            <p>We'll initiate payment after your item has been inspected and accepted.</p>
                        </div>
                        <div class="payment-options-list">
                            <div ng-click="vm.step2.selectMethod('amazon')"
                                 ng-class="{'selected': vm.step2.methods.amazon.selected}"
                                 class="selectable-thumb payment-option">
                                <img src="images/amazon.png"/>
                                <h4>Amazon gift card</h4>

                                <p>Get paid quickly via electronic gift card and receive an extra 5%</p>
                            </div>
                            <div ng-click="vm.step2.selectMethod('paypal')"
                                 ng-class="{'selected': vm.step2.methods.paypal.selected}"
                                 class="selectable-thumb payment-option">
                                <img src="images/paypal.png"/>
                                <h4>Paypal</h4>

                                <p>No need to wait for the mail. Get your payment quickly through your Paypal
                                    account</p>
                            </div>
                            <div ng-click="vm.step2.selectMethod('check')"
                                 ng-class="{'selected': vm.step2.methods.check.selected}"
                                 class="selectable-thumb payment-option">
                                <img src="images/check.png"/>
                                <h4>Check</h4>

                                <p>Get paid via standard bank check, delivered by mail within 10 days</p>
                            </div>
                            <div ng-click="vm.step2.selectMethod('charity')"
                                 ng-class="{'selected': vm.step2.methods.charity.selected}"
                                 class="selectable-thumb payment-option">
                                <img src="images/charity.png"/>
                                <h4>Donate to charity</h4>

                                <p>Give the value of your items to a good cause</p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="alert alert-info" ng-if="vm.step2.methods.amazon.selected">
                            Your gift card will be sent to your email: @{{ vm.user.email }}
                        </div>
                        <div class="step-fields">
                            <form name="form2" novalidate>

                                <div ng-if="vm.step2.methods.paypal.selected">
                                    Enter the email address associated with your paypal account
                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating2-label form-field">
                                        <input class="mdl-textfield__input" type="email"
                                               ng-model="vm.step2.methods.paypal.email" name="paypal_email"
                                               id="paypal_email" required>
                                        <label class="mdl-textfield__label" for="paypal_email">Paypal Email
                                            Address</label>
                                    </div>
                                </div>
                                <div ng-if="vm.step2.methods.check.selected">
                                    <small>Please provide the address you'd like your check sent to.</small>
                                    <div style="width:100%;margin-left:0%;"
                                         class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label form-field">
                                        <input class="mdl-textfield__input" type="text" id="payee_name-ff"
                                               name="payee_name"
                                               ng-model="vm.step2.methods.check.payee_name" required>
                                        <label class="mdl-textfield__label" for="payee_name-ff">Payable To</label>
                                    </div>
                                    <div style="width:44%;margin-left:0%;"
                                         class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label form-field">
                                        <input class="mdl-textfield__input" type="text" id="address1-ff" name="address1"
                                               ng-model="vm.step2.methods.check.address1" required>
                                        <label class="mdl-textfield__label" for="address1-ff">Address Line 1</label>
                                    </div>
                                    <div style="width:44%;margin-left:6%;"
                                         class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label form-field">
                                        <input class="mdl-textfield__input" type="text" id="address2-ff" name="address2"
                                               ng-model="vm.step2.methods.check.address2">
                                        <label class="mdl-textfield__label" for="address2-ff">Address Line 2</label>
                                    </div>
                                    <div style="width:44%;margin-left:0%;"
                                         class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label form-field">
                                        <input class="mdl-textfield__input" type="text" id="city-ff" name="city"
                                               ng-model="vm.step2.methods.check.city" required>
                                        <label class="mdl-textfield__label" for="city-ff">City</label>
                                    </div>
                                    <div style="width:44%;margin-left:6%;"
                                         class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label form-field">
                                        <input class="mdl-textfield__input" type="text" id="state-ff" name="state"
                                               ng-model="vm.step2.methods.check.state" required>
                                        <label class="mdl-textfield__label" for="state-ff">State</label>
                                    </div>
                                    <div style="width:44%;margin-left:0%;"
                                         class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label form-field">
                                        <input class="mdl-textfield__input" type="text" id="zip-ff" name="zip"
                                               ng-model="vm.step2.methods.check.zip" required>
                                        <label class="mdl-textfield__label" for="zip-ff">Zip Code</label>
                                    </div>
                                    <div style="width:44%;margin-left:6%;"
                                         class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label form-field">
                                        <input class="mdl-textfield__input" type="text" id="country-ff" name="country"
                                               ng-model="vm.step2.methods.check.country" value="United States" disabled
                                               required>
                                        <label class="mdl-textfield__label" for="country-ff">Country</label>
                                    </div>
                                    <div style="width:100%;margin-left:0%;"
                                         class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label form-field">
                                        <input class="mdl-textfield__input" type="text" id="phone-ff" name="phone"
                                               ng-model="vm.step2.methods.check.phone" required>
                                        <label class="mdl-textfield__label" for="phone-ff">Phone Number</label>
                                    </div>
                                </div>
                                <div style="margin: 10px 0px 20px 0px;" ng-if="vm.step2.methods.charity.selected">
                                    <small>Choose a charity from the list below</small>
                                    <select ng-model="vm.step2.methods.charity.charity_name" name="charity" required>
                                        <option>Choose a charity</option>
                                    </select>
                                </div>
                            </form>
                        </div>
                        <div class="step-actions">
                            <button ng-click="vm.step2.onSubmit()"
                                    ng-disabled="form2.$invalid||(!vm.step2.selectedMethod)||vm.uiBusy"
                                    class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect">
                                Continue >>
                            </button>
                        </div>
                    </div>
                    <div ng-show="vm.step3.visible" id="step3" class="step-container">
                        <div class="step-header">
                            <h3>Provide your shipping address</h3>

                            <p>We use this information to create your shipping labels so you can send your item to us
                                for free!</p>
                        </div>
                        <div ng-show="vm.step3.error" class="alert alert-danger">@{{ vm.step3.error }}</div>
                        <div class="step-fields">
                            <form novalidate name="form3">
                                <div style="width:44%;margin-left:0%;"
                                     class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label form-field">
                                    <input class="mdl-textfield__input" type="text" id="first_name-ff" name="first_name"
                                           ng-model="vm.step3.user.first_name" required>
                                    <label class="mdl-textfield__label" for="first_name-ff">First Name</label>
                                </div>
                                <div style="width:44%;margin-left:6%;"
                                     class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label form-field">
                                    <input class="mdl-textfield__input" type="text" id="last_name-ff" name="last_name"
                                           ng-model="vm.step3.user.last_name" required>
                                    <label class="mdl-textfield__label" for="last_name-ff">Last Name</label>
                                </div>
                                <div style="width:44%;margin-left:0%;"
                                     class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label form-field">
                                    <input class="mdl-textfield__input" type="text" id="address1-ff" name="address1"
                                           ng-model="vm.step3.user.address1" required>
                                    <label class="mdl-textfield__label" for="address1-ff">Address Line 1</label>
                                </div>
                                <div style="width:44%;margin-left:6%;"
                                     class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label form-field">
                                    <input class="mdl-textfield__input" type="text" id="address2-ff" name="address2"
                                           ng-model="vm.step3.user.address2">
                                    <label class="mdl-textfield__label" for="address2-ff">Address Line 2</label>
                                </div>
                                <div style="width:44%;margin-left:0%;"
                                     class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label form-field">
                                    <input class="mdl-textfield__input" type="text" id="city-ff" name="city"
                                           ng-model="vm.step3.user.city" required>
                                    <label class="mdl-textfield__label" for="city-ff">City</label>
                                </div>
                                <div style="width:44%;margin-left:6%;"
                                     class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label form-field">
                                    <input class="mdl-textfield__input" type="text" id="state-ff" name="state"
                                           ng-model="vm.step3.user.state" required>
                                    <label class="mdl-textfield__label" for="state-ff">State</label>
                                </div>
                                <div style="width:44%;margin-left:0%;"
                                     class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label form-field">
                                    <input class="mdl-textfield__input" type="text" id="zip-ff" name="zip"
                                           ng-model="vm.step3.user.zip" required>
                                    <label class="mdl-textfield__label" for="zip-ff">Zip Code</label>
                                </div>
                                <div style="width:44%;margin-left:6%;"
                                     class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label form-field">
                                    <input class="mdl-textfield__input" type="text" id="country-ff" name="country"
                                           ng-model="vm.step3.user.country" value="United States" disabled required>
                                    <label class="mdl-textfield__label" for="country-ff">Country</label>
                                </div>
                                <div style="width:100%;margin-left:0%;"
                                     class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label form-field">
                                    <input class="mdl-textfield__input" type="text" id="phone-ff" name="phone"
                                           ng-model="vm.step3.user.phone" required>
                                    <label class="mdl-textfield__label" for="phone-ff">Phone Number</label>
                                </div>
                            </form>
                        </div>
                        <div class="step-header sub">
                            <h3>Choose your shipping method</h3>

                            <p>How would you like to receive shipping label?</p>
                        </div>
                        <div class="step-fields shipping-options">
                            <div ng-class="{'selected':vm.step3.user.shipping_method==='box'}"
                                 ng-click="vm.step3.setShippingMethod('box')" class="selectable-thumb shipping-option">
                                <img src="images/shipping-box.png"/>
                                <h4>A box and a prepaid label</h4>

                                <p>We’ll send a prepaid shipping label and a box right to your house.</p>
                            </div>
                            <div ng-class="{'selected':vm.step3.user.shipping_method==='fedex'}"
                                 ng-click="vm.step3.setShippingMethod('fedex')"
                                 class="selectable-thumb shipping-option">
                                <img src="images/fedex.png"/>
                                <h4>Prepaid FedEx Shipping Label</h4>

                                <p>We’ll send a prepaid shipping label and a box right to your house.</p>
                            </div>
                            <div ng-class="{'selected':vm.step3.user.shipping_method==='usps'}"
                                 ng-click="vm.step3.setShippingMethod('usps')" class="selectable-thumb shipping-option">
                                <img src="images/usps.png"/>
                                <h4>Prepaid USPS Shipping Label</h4>

                                <p>We’ll send a prepaid shipping label and a box right to your house.</p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="step-actions">
                            <button ng-click="vm.step3.onSubmit()"
                                    ng-disabled="form3.$invalid||vm.uiBusy"
                                    class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect">
                                Continue >>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="mdl-cell mdl-cell--4-col cart-items-container">
                    <div class="checkout-details-wrapper">
                        <ul class="cart-items-list">
                            <li ng-repeat="item in vm.cart.items"
                                class="cart-menu-item">
                                <div class="img-container">
                                    <img ng-src="@{{item.img_url}}"/>
                                </div>

                                <div class="info">
                                    <h3>@{{item.title|limit:14}}</h3>

                                    <p>Value: $@{{item.value}}</p>
                                    <a ng-click="vm.cart.removeItem(item)"
                                       href="javascript:void(0)">Remove
                                        Item</a>
                                </div>
                                <div class="clearfix"></div>
                            </li>
                        </ul>
                        <div class="total-value-container">
                            Total: $@{{vm.cart.getTotal()}}
                        </div>
                        <div class="add-more-button-container">
                            <a href="brands" class="add-more-button"><< add another item</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
