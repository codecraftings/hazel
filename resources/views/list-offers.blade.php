@extends('app')
@section("window-title")
    Your Offers
@stop
@section('content')
    <div class="grid-container-wrapper">
        <div class="container">
            <div
                    class="offers-list-container confirmation-page-container flat-grid-container mdl-card mdl-shadow--2dp mdl-grid">
                <div class="mdl-card__title">
                    <div class="mdl-card__title-text">Your Offers</div>
                </div>
                <div class="list-group mdl-cell mdl-cell--12-col">
                    @foreach($offers as $offer)
                        <a href="{{$offer->permalink}}" class="list-group-item">
                            <img src="{{$offer->img_url}}"/>

                            <div class="info">
                                {{$offer->title}}<br>
                                ${{$offer->total_value}}<br>
                                Status: {{ucwords(str_replace("_"," ", $offer->status))}}
                            </div>
                            <div class="clearfix"></div>
                        </a>
                    @endforeach
                </div>
                @if(count($offers)<1)
                    <div class="alert alert-info">Sorry you didn't offer anything ever!</div>
                @endif
            </div>
        </div>
    </div>
@stop