(function (){
   angular.module('app.user').service('currentUser', CurrentUser);
   
   CurrentUser.$inject = [];
   function CurrentUser(){
      var user = {};
      if(typeof userDataFromServer==="undefined"){
         user.loggedIn = false;
      }else{
         user = userDataFromServer;
         user.loggedIn = true;
      }
      user.setUser = setUser;
      user.getPaymentMethod = getPaymentMethod;
      user.isLoggedIn = isLoggedIn;
      return user;

      function setUser(data){
         angular.extend(user, data);
         user.loggedIn = true;
      }
      function isLoggedIn(){
         return user.loggedIn;
      }
      function getPaymentMethod(){
         if(!user.payment_method||angular.isUndefined(user.payment_method.name)){
            return null;
         }
         return user.payment_method;
      }
   }
})();