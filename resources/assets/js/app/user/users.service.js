(function (){
   angular.module('app.user').service('users', UsersRepo);
   
   UsersRepo.$inject = [ '$q', '$http', 'urls', 'currentUser' ];
   function UsersRepo($q, $http, urls, currentUser){
      var _this = this;
      _this.login = login;
      _this.signup = signup;
      _this.update = update;
      _this.ifEmailExists = ifEmailExists;

      function ifEmailExists(email){
         var df = $q.defer();
         $http.post(urls.api("email/check"), { email: email })
            .success(function (response){
               if ( response.email_exists ) {
                  df.resolve();
               } else {
                  df.reject();
               }
            })
            .catch(function (){

            })
         return df.promise;
      }

      function login(email, password){
         var df = $q.defer();
         $http.post(urls.api("ajax/login"), { email: email, password: password })
            .success(function(response){
               currentUser.setUser(response.user);
               df.resolve();
            })
            .error(function(response){
               df.reject(response.error);
            })
         return df.promise;
      }

      function signup(email, password){
         var df = $q.defer();
         $http.post(urls.api("ajax/signup"), { email: email, password: password })
            .success(function(response){
               currentUser.setUser(response.user);
               df.resolve();
            })
            .error(function(response){
               df.reject(response.error);
            })
         return df.promise;
      }

      function update(data){
         var df = $q.defer();
         $http.post(urls.api("ajax/update"), data)
            .success(function(response){
               currentUser.setUser(response.user);
               df.resolve();
            })
            .error(function(response){
               df.reject(response.error);
            })
         return df.promise;
      }

      return _this;
   }
})();