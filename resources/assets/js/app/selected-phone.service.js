(function (){
   angular.module('app').service('selectedPhone', SelectedPhone);
   
   SelectedPhone.$inject = [];
   function SelectedPhone(){
      var phone = phoneDataFromServer;
      phone.phone_id = phone.id;
      angular.forEach(phone.questions, function(question, index){
         question.index = index+1;
      });
      return phone;
   }
})();