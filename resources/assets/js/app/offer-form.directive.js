(function (){
   angular.module('app').directive('offerForm', registerDirective);
   
   registerDirective.$inject = [];
   function registerDirective(){
      var directive = {
         link        : link,
         scope       : {
            alias        : "=?",
            enterEditMode: '&'
         },
         restrict    : 'EA',
         controller  : Controller,
         controllerAs: "vm",
         templateUrl : "components/offer-form.html"
      };
      return directive;
      function link($scope, elm, attr){
         
      }
   }
   
   Controller.$inject = [ '$scope', '$attrs', '$http', 'currentUser', 'urls', '$timeout' ]
   function Controller(scope, attr, $http, currentUser, urls, $timeout){
      var api = this;
      scope.alias = api;
      scope.formVisible = false;
      scope.successNoticeVisible = false;
      scope.submitDisabled = true;
      scope.loginMode = false;
      scope.registerMode = false;
      scope.formData = {};
      api.grabOffer = grabOffer;
      api.checkEmailExists = checkEmailExists;
      scope.submitForm = submitForm;
      if ( ! currentUser.loggedIn ) {
         scope.user = {};
         scope.userLoggedIn = false;
      } else {
         scope.user = angular.copy(currentUser);
         scope.userLoggedIn = true;
      }
      function grabOffer(offerData){
         scope.formVisible = true;
         scope.successNoticeVisible = false;
         scope.formData.offer = offerData;
         scope.error = "";
         scope.submissionPending = false;
      }

      function submitForm(e){
         e.preventDefault();
         scope.formData.user = scope.user;
         if ( scope.loginMode ) {
            scope.formData.loginMode = true;
         }
         else if ( scope.registerMode ) {
            scope.formData.registerMode = true;
         }
         else if(currentUser.loggedIn){
            scope.formData.alreadyLoggedIn = true;
         }
         scope.submissionPending = true;
         $http.post(urls.api("offer/grab"), scope.formData)
            .success(function (response){
               scope.formVisible = false;
               scope.successNoticeVisible = true;
               scope.error = "";
            })
            .error(function (response){
               console.log(response);
               if ( response.error ) {
                  scope.error = response.error;
               } else {
                  scope.error = "Something went wrong";
               }
            })
            .finally(function (){
               scope.submissionPending = false;
            });
      }

      function checkEmailExists(){
         var exp = /^[\S]+@[\S]+\.[A-z0-9]+$/;
         if ( ! exp.test(scope.user.email) ) {
            return;
         }
         if ( scope.userLoggedIn && scope.user.email === currentUser.email ) {
            return;
         }
         scope.loginMode = false;
         scope.registerMode = false;
         scope.emailLoaderVisible = true;
         $http.post(urls.api("email/check"), { email: scope.user.email })
            .success(function (response){
               if ( response.email !== scope.user.email ) {
                  return;
               }
               if ( response.email_exists ) {
                  scope.loginMode = true;
               } else {
                  console.log(scope.userLoggedIn);
                  if ( ! scope.userLoggedIn ) {
                     scope.registerMode = true;
                  }
               }
               $timeout(function (){
                  componentHandler.upgradeDom();
               }, 500);
               scope.emailLoaderVisible = false;
            })
            .catch(function (){
               $timeout(function (){
                  checkEmailExists();
               }, 2000);
            });
      }
   }
})();