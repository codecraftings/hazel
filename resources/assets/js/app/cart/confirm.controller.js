(function (){
   angular.module('cart').controller('CheckoutConfirmationController', CheckoutConfirmationController);
   
   CheckoutConfirmationController.$inject = ['cart', 'currentUser','$location','$http','urls'];
   function CheckoutConfirmationController(cart, currentUser, $location, $http, urls){
      var vm = this;
      vm.cart = cart;
      if($location.path().match(/offer\/\d+/)){
         window.location.href = $location.path();
         return;
      }
      if(cart.items.length<1||!currentUser.isLoggedIn()||!currentUser.getPaymentMethod()){
         window.location.href = "/";
         return;
      }
      vm.offerData = prepareOfferData(cart, currentUser);
      vm.copiedOfferData = angular.copy(vm.offerData);
      vm.confirm = confirm;
      vm.confirmed = false;
      function confirm(){
         vm.confirming = true;
         vm.error = "";
         $http.post(urls.api("checkout/confirm"), vm.offerData)
            .success(function(response){
               cart.empty();
               vm.confirmed = true;
               $location.path("offer/"+response.id).replace();
            })
            .error(function(response){
               vm.error = "Oh snap! Something went wrong!";
            })
            .finally(function(){
               vm.confirming = false;
            })
      }
   }
   function prepareOfferData(cart, currentUser){
      var offer = {};
      offer.items = pickItems(cart.items);
      offer.total_value = cart.getTotal();
      offer.shipping_address = pick(currentUser, "first_name","last_name", "address1","address2","city","state","zip","country", "phone");
      offer.shipping_method = currentUser.shipping_method;
      offer.contact_email = currentUser.email;
      offer.payment_method = currentUser.getPaymentMethod();
      console.log(offer);
      return offer;
   }
   function pickItems(items){
      var pickedItems = [];
      angular.forEach(items,function(item){
         var o = pick(item, "id", "phone_id", "value", "name", "img_url","permalink");
         o.questions = [];
         angular.forEach(item.questions, function(question){
            var q = pick(question, "index", "title");
            q.answer = question.selectedOption.label;
            o.questions.push(q);
         });
         pickedItems.push(o);
      });
      return pickedItems;
   }
   function pick(){
      var obj = arguments[0];
      var out = {};
      var keys = Array.prototype.slice.call(arguments, 1);
      for(var propName in obj){
         if(keys.indexOf(propName)>(-1)){
            out[propName] = obj[propName];
         }
      }
      return out;
   }
})();