(function (){
   angular.module('cart').directive('cartButton', cartButton);
   
   cartButton.$inject = ['cart'];
   function cartButton(cart){
      var directive = {
         scope      : {},
         link       : link,
         restrict   : 'EA',
         templateUrl: 'components/cart-button.html'
      };
      return directive;
      
      function link(scope, elm, attr){
         scope.cart = cart;
         componentHandler.upgradeDom();
      }
   }
})();