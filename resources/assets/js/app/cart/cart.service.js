(function (){
   angular.module('cart').service('cart', CartService);
   
   CartService.$inject = ['$localStorage'];
   function CartService($localStorage){
      var _this = this;
      var items = $localStorage.cartItems;
      console.log(items);
      _this.items = angular.isDefined(items)?items:[];
      _this.checkoutPage = false;

      _this.removeItem = removeItem;

      _this.addItem = addItem;

      _this.getTotal = getTotal;
      _this.empty = empty;

      function empty(){
         _this.items.length = 0;
      }
      function getTotal(){
         var total = 0;
         angular.forEach(_this.items, function(item){
            total += item.value;
         });
         return total;
      }
      function removeItem(item){
         var items = angular.copy(_this.items);
         _this.items.length = 0;
         angular.forEach(items, function (i){
            if ( !(item.id === i.id && item.value=== i.value) ) {
               _this.items.push(i);
            }
         });
         saveToCookie();
      }

      function addItem(item){
         _this.items.push(item);
         saveToCookie();
      }

      function saveToCookie(){
         $localStorage.cartItems = _this.items;
      }
      return _this;
   }
})();