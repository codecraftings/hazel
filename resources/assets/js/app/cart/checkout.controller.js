(function (){
   angular.module('cart').controller('CheckoutController', CheckoutController);

   var navigate;
   var showBusy;
   var hideBusy;
   CheckoutController.$inject = [ 'cart', 'users', 'currentUser', '$timeout','$location' ];
   function CheckoutController(cart, users, currentUser, $timeout, $location){
      var vm = this;
      vm.cart = cart;
      vm.user = currentUser;
      vm.step1 = new StepOneController(users, currentUser, $timeout)
      vm.step2 = new StepTwoController($timeout, currentUser, users);
      vm.step3 = new StepThreeController($timeout, currentUser, users, $location);
      navigate = goTo;
      showBusy = showBusyIndicator;
      hideBusy = hideBusyIndicator;
      vm.navigate = navigate;
      if(cart.items.length<1){
         window.location.href = "/";
         return;
      }
      if ( ! currentUser.isLoggedIn() ) {
         navigate(1);
      }
      else if($location.path()==="/step2"){
         navigate(2);
      }
      else if ( currentUser.getPaymentMethod() === null ) {
         navigate(2);
      }
      else {
         navigate(3);
      }
      function showBusyIndicator(){
         vm.uiBusy = true;
      }

      function hideBusyIndicator(){
         vm.uiBusy = false;
      }

      function goTo(stepNum){
         checkAccessLevel();
         var to = vm[ "step" + stepNum ];
         vm.step1.visible = false;
         vm.step2.visible = false;
         vm.step3.visible = false;
         to.visible = true;
         to.disabled = true;
         to.onEnter();
      }

      function checkAccessLevel(){
         vm.step1.disabled = false;
         vm.step2.disabled = false;
         vm.step3.disabled = false;
         if ( currentUser.isLoggedIn() ) {
            vm.step1.disabled = true;
            if ( currentUser.getPaymentMethod() === null ) {
               vm.step3.disabled = true;
            }
         } else {
            vm.step2.disabled = true;
            vm.step3.disabled = true;
         }
      }
   }

   function StepTwoController($timeout, currentUser, users){
      var vm = this;
      vm.selectedMethod = false;
      vm.selectMethod = selectMethod;
      vm.onEnter = onEnter;
      vm.onSubmit = onSubmit;
      vm.methods = {
         amazon : {},
         paypal : {},
         check  : {country: "United States"},
         charity: {}
      };
      function onSubmit(){
         vm.error = "";
         showBusy();
         users.update({ payment_method: vm.selectedMethod })
            .then(function (){
               navigate(3);
            })
            .catch(function (error){
               vm.error = error;
            })
            .finally(function (){
               hideBusy();
            });
      }

      function onEnter(){
         console.log(currentUser);
         var method = currentUser.getPaymentMethod();
         if ( method !== null ) {
            angular.extend(vm.methods[ method.name ], method);
            vm.selectMethod(method.name);
         }
         $timeout(componentHandler.upgradeDom, 100);
      }

      function selectMethod(name){
         angular.forEach(vm.methods, function (method){
            method.selected = false;
         });
         var selected = vm.methods[ name ];
         selected.selected = true;
         vm.selectedMethod = selected;
         vm.selectedMethod.name = name;
         $timeout(componentHandler.upgradeDom, 100);
      }
   }

   function StepThreeController($timeout, currentUser, users, $location){
      var vm = this;
      vm.setShippingMethod = function(name){
         vm.user.shipping_method = name;
      }
      vm.user = angular.copy(currentUser);
      vm.onEnter = function (){
         angular.extend(vm.user, currentUser);
         if(!vm.user.shipping_method){
            vm.user.shipping_method = "box";
         }
         vm.user.country = "United States";
         $timeout(componentHandler.upgradeDom, 100);
      };
      vm.onSubmit = function (){
         vm.error = "";
         showBusy();
         users.update(vm.user).then(function (){
            //$location.path("checkout/confirm").replace();
            window.location.href = "checkout/confirm";
         })
            .catch(function (error){
               if(!error||!angular.isString(error)){
                  vm.error = "Something went wrong!";
               }else{
                  vm.error = error;
               }
               hideBusy();
            })
      }
   }

   function StepOneController(users, currentUser, $timeout){
      var vm = this;
      vm.onEnter = function (){

      }
      vm.loginMode = false;
      vm.singupMode = false;
      vm.onEmailChange = onEmailChange;
      vm.checkIfEmailExists = checkIfEmailExists;
      vm.submit = submit;
      var timer = null;

      function submit(){
         var promise;
         if ( vm.loginMode ) {
            promise = users.login(vm.email, vm.password);
         }
         if ( vm.signupMode ) {
            promise = users.signup(vm.email, vm.new_password);
         }
         vm.error = "";
         showBusy();
         promise.then(function (){
            navigate(2);
         }).catch(function (error){
            vm.error = error;
         })
            .finally(function (){
               hideBusy();
            });
      }

      function onEmailChange(event){
         $timeout.cancel(timer);
         timer = $timeout(function (){
            vm.checkIfEmailExists(vm.email)
         }, 500);
      }

      function checkIfEmailExists(email){
         vm.loginMode = false;
         vm.signupMode = false;
         var exp = /^[\S]+@[\S]+\.[A-z0-9]+$/;
         if ( ! exp.test(email) ) {
            return;
         }
         vm.loaderVisible = true;
         users.ifEmailExists(email).then(function (){
            if ( email !== vm.email ) {
               return;
            }
            vm.loginMode = true;
            vm.signupMode = false;
         }).catch(function (){
            if ( email !== vm.email ) {
               return;
            }
            vm.signupMode = true;
            vm.loginMode = false;
         }).finally(function (){
            vm.loaderVisible = false;
            $timeout(componentHandler.upgradeDom, 0);
         })
      }
   }
})();