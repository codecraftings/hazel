(function (){
   angular.module('app.core').directive('mdlCheckbox', mdlCheckbox);
   
   mdlCheckbox.$inject = [];
   function mdlCheckbox(){
      var n = 1;
      var directive = {
         scope      : {
            id   : "@",
            name : "@",
            value: "@",
            label: "@",
            dModel: "=?"
         },
         link       : link,
         restrict   : 'EA',
         templateUrl: 'components/checkbox.html'
      };
      return directive;
      
      function link(scope, elm, attr){
         if ( angular.isUndefined(attr.id) ) {
            scope.id = "checkbox-" + n;
            n ++;
         }
         componentHandler.upgradeDom();
      }
   }
})();