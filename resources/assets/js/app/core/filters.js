(function (){
   angular.module('app.core')
      .filter('limit', function (){
         return function (input, maxlen, trail){
            if ( angular.isUndefined(trail) ) {
               trail = "..";
            }
            var out = input;
            if ( angular.isDefined(input)&&input.length > maxlen ) {
               out = out.substr(0, maxlen - trail.length);
               out += trail;
            }
            return out;
         }
      })
      .filter('ifMany', function (){
         return function(num, $plural, $singular){
            var n = parseInt(num);
            var out = "";
            if(n===1){
               out = $singular;
            }
            else{
               out = $plural;
            }
            out = out.replace(":i ", num + " ");
            return out;
         }
      });
})();