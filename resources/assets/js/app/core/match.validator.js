(function (){
   angular.module('app.core').directive('match', match);
   
   match.$inject = [];
   function match(){
      var directive = {
         require : [ 'ngModel', '^form' ],
         link    : link,
         restrict: 'A'
      };
      return directive;
      
      function link(scope, elm, attr, ctrls){
         var model = ctrls[ 0 ];
         var form = ctrls[ 1 ];
         var otherField = form[ attr.match ];
         var otherFieldValue = function(){
            return otherField.$viewValue;
         }
         scope.$watch(otherFieldValue, function(){
            model.$validate();
         })
         model.$validators.$match = function (modelValue, viewValue){
            if ( viewValue === otherField.$viewValue ) {
               return true;
            } else {
               return false;
            }
         }
      }
   }
})();