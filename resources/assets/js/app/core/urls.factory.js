(function (){
    angular.module('app.core')
       .config(['$locationProvider', function($locationProvider){
           //$locationProvider.html5Mode(true);
       }])
        .constant('base_url', {
                          api: window.api_url,
                          base: window.base_url,
                          asset: window.asset_url
                      })
        .factory('urls', urlsFactory)
        .filter('url', urlFilterProvider);

    urlsFactory.$inject = ['base_url'];
    function urlsFactory(base_url){
        return {
            pending_path: false,
            base: base,
            api: api,
            asset: asset
        }
        function base(path){
            return make_url(base_url.base, path);
        }
        function api(path){
            return make_url(base_url.api, path);
        }
        function asset(path){
            return make_url(base_url.asset, path);
        }
        function make_url(basePath, path){
            if(path[0]==='/'){
                if(basePath[basePath.length-1]==='/'){
                    return basePath+path.substr(1, path.length-1);
                }
            }else{
                if(basePath[basePath.length-1]!=='/'){
                    return basePath+'/'+path;
                }
            }
            return basePath+path;
        }
    }
    urlFilterProvider.$inject = ['urls'];
    function urlFilterProvider(urls){
        return function(path, type){
            if(angular.isUndefined(type)){
                type = "asset";
            }
            var out = "";
            switch(type){
                case 'asset':
                    out = urls.asset(path);
                    break;
                case 'api':
                    out = urls.api(path);
                case 'base':
                    out = urls.base(path);
                    break;
            }
            return out;
        }
    }
})();