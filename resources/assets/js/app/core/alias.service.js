(function (){
   angular.module('app.core').service('alias', AliasService);
   
   AliasService.$inject = [];
   function AliasService(){
      this.bindApi = _bind;

      function _bind(scope, alias, api){
         if ( angular.isUndefined(alias) ) {
            return api;
         }
         if ( scope.$eval(alias) === undefined ) {
            scope.$eval(alias + "={}");
         }
         alias = scope.$eval(alias);
         angular.extend(alias, api);
         return api;
      }
   }
})();