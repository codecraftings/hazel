(function (){
   angular.module('app.core').directive('mdlTextfield', mdlTextfield);
   
   mdlTextfield.$inject = [];
   function mdlTextfield(){
      var n = 1;
      var directive = {
         scope      : {
            id       : "@",
            name     : "@",
            label    : "@",
            dModel   : '=?',
            dRequired: '=?',
            type     : "@"
         },
         link       : link,
         restrict   : 'EA',
         templateUrl: 'components/textfield.html'
      };
      return directive;
      
      function link(scope, elm, attr){
         if ( angular.isUndefined(attr.id) ) {
            scope.id = "tf-" + n;
            n ++;
         }
         attr.$observe("dRequired", function (){
            var inp = elm.find('input');
            if ( attr.dRequired === true ) {
               console.log("f");
               inp.attr("required", true);
            } else {
               inp.removeAttr("required");
            }
         })
         componentHandler.upgradeDom();
      }
   }
})();