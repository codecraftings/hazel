(function (){
   angular.module('offers').directive('offerDetails', offerDetails);
   
   offerDetails.$inject = [];
   function offerDetails(){
      var directive = {
         scope      : {
            offer: "=offerDetails"
         },
         link       : link,
         restrict   : 'EA',
         templateUrl: 'components/offer-details.html'
      };
      return directive;
      
      function link(scope, elm, attr){
         
      }
   }
})();