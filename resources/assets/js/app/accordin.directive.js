(function (){
   angular.module('app').directive('accordin', accordin);
   
   accordin.$inject = ['$location'];
   function accordin($location){
      var directive = {
         scope      : {},
         link       : link,
         restrict   : 'EA',
         templateUrl: ''
      };
      return directive;
      
      function link(scope, elm, attr){
         console.log($location.search());
         var titleContainer = angular.element(elm[ 0 ].querySelector(".accordin-title"));
         var contentContainer = angular.element(elm[ 0 ].querySelector(".accordin-content"));
         var iconOpen = angular.element("<i class='material-icons opened'>indeterminate_check_box</i>");
         var iconClosed = angular.element("<i class='material-icons closed'>add_box</i>");
         titleContainer[ 0 ].insertBefore(iconOpen[ 0 ], titleContainer[ 0 ].firstChild);
         titleContainer[ 0 ].insertBefore(iconClosed[ 0 ], titleContainer[ 0 ].firstChild);
         contentContainer.addClass("hidden");
         elm.addClass("collapsed");
         titleContainer.addClass("collapsed");
         titleContainer.on("click", function (){
            contentContainer.toggleClass("hidden");
            elm.toggleClass("collapsed");
            titleContainer.toggleClass("collapsed");
         });
      }
   }
})();