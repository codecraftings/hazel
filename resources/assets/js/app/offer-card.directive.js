(function (){
   angular.module('app').directive('offerCard', registerDirective);
   
   registerDirective.$inject = [];
   function registerDirective(){
      var directive = {
         link      : link,
         scope: {
            alias: "=?",
            enterEditMode: '&',
            onOfferGrabbed: '&'
         },
         restrict  : 'EA',
         controller: Controller,
         controllerAs: "vm",
         templateUrl: "components/offer-card.html"
      };
      return directive;
      function link($scope, elm, attr){
         
      }
   }
   
   Controller.$inject = [ '$scope', '$attrs', 'alias','$timeout' ]
   function Controller(scope, attr, alias, $timeout){
      var api = this;
      scope.alias = api;
      scope.data = {};
      scope.offerLoaded = false;
      api.loadOffeer = loadOffer;
      scope.grabOffer = grabOffer;

      function grabOffer(){
         scope.buttonsDisabled = true;
         scope.onOfferGrabbed({offerData: scope.data});
      }
      function loadOffer(phoneData){
         scope.offerLoaded = false;
         scope.buttonsDisabled = false;
         $timeout(function(){
            scope.offerLoaded = true;
            scope.data = phoneData;
         }, 1000);
      }
   }
})();