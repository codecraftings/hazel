(function (){
   angular.module('app.questions').directive('opt', qOption);
   
   qOption.$inject = ['$timeout'];
   function qOption($timeout){
      var directive = {
         scope      : {
            selectedValue: '=',
            optId   : '@',
            optName : '@',
            optValue: '@',
            optLabel: '@'
         },
         link       : link,
         restrict   : 'EA',
         templateUrl: 'components/q-option.html'
      };
      return directive;
      
      function link(scope, elm, attr){
         //componentHandler.upgradeElement(elm.child('label'), MaterialCheckbox);
         //scope.checked = false;
         componentHandler.upgradeDom();
      }
   }
})();