(function (){
   angular.module('app.questions').controller('QuestionsController', QuestionsController);
   
   QuestionsController.$inject = [ '$anchorScroll', 'selectedPhone', '$timeout', 'cart' ];
   function QuestionsController($anchorScroll, selectedPhone, $timeout, cart){
      var vm = this;
      vm.editMode = true;
      vm.finalCalculateButtonEnabled = false;
      vm.handleQuestionAnswered = handleQuestionAnswered;
      vm.calculateFinalOffer = calculateFinalOffer;
      var phoneData = angular.copy(selectedPhone);
      var _base_price = phoneData.base_price;
      vm.estimatedOffer = _base_price;
      vm.questions = phoneData.questions;
      vm.currentQuestion = vm.questions[0];
      vm.grabOffer = grabOffer;
      vm.setSelectedOpt = setSelectedOpt;
      vm.lastQuestion = lastQuestion;

      function lastQuestion(){
         var index = vm.currentQuestion.index-2;
         if(index>=0){
            vm.currentQuestion = vm.questions[index];
         }
      }

      function grabOffer(offerData){
         var item = offerData;
         item.title = offerData.name;
         item.value = offerData.finalOffer;
         cart.addItem(item);
         window.location.href="checkout";
      }
      function calculateFinalOffer(){
         $anchorScroll("final_offer_card");
         vm.editMode = false;
         phoneData.finalOffer = vm.estimatedOffer;
         vm.offerCard.loadOffeer(phoneData);
      }
      function setSelectedOpt(q, opt){
         angular.forEach(q.options, function(o){
            o.selected = false;
         });
         opt.selected = true;
         q.selectedOption = opt;
         calculateEstimatePrice();
         if( q.index===vm.questions.length){
            showFinalOffer();
         }else{
            $timeout(showNextQuestion, 200);
         }
      }
      function showNextQuestion(){
         vm.currentQuestion = vm.questions[vm.currentQuestion.index];
      }
      function showFinalOffer(){
         calculateFinalOffer();
         componentHandler.upgradeDom();
      }
      function handleQuestionAnswered(q){
         console.log(q);
         calculateEstimatePrice();
         if ( allQuestionAnswered() ) {
            vm.finalCalculateButtonEnabled = true;
            return;
         }
         vm.questions[ q.index ].disabled = false;
      }

      function calculateEstimatePrice(){
         var offer = parseFloat(_base_price);
         angular.forEach(vm.questions, function (q){
            if ( angular.isUndefined(q.selectedOption) ) {
               return;
            }
            if ( q.selectedOption.add ) {
               offer += parseFloat(q.selectedOption.add);
            }
            if ( q.selectedOption.minus ) {
               offer -= parseFloat(q.selectedOption.minus);
            }
         });
         vm.estimatedOffer = offer;
      }

      function allQuestionAnswered(){
         var flag = true;
         angular.forEach(vm.questions, function (q){
            if ( ! q.selectedValue ) {
               flag = false;
            }
         });
         return flag;
      }
   }
})();