(function (){
   angular.module('app.questions').directive('question', question);
   
   question.$inject = [];
   function question(){
      var directive = {
         scope      : {
            data: "=",
            onChoiceChanged: "&"
         },
         link       : link,
         restrict   : 'EA',
         templateUrl: "components/question.html"
      };
      return directive;
      
      function link(scope, elm, attr){
         scope.$watch("data.selectedValue", function(newValue){
            if(angular.isUndefined(newValue)){
               return;
            }
            var selectedOption = {};
            angular.forEach(scope.data.options, function(opt){
               if(opt.value===newValue){
                  selectedOption = opt;
               }
            });
            scope.data.selectedOption = selectedOption;
            if(angular.isDefined(attr.onChoiceChanged)){
               scope.onChoiceChanged({question:scope.data});
            }
         });
      }
   }
})();