(function (){
   angular.module('app').directive('navbarModel', navbar);
   
   navbar.$inject = ['currentUser'];
   function navbar(currentUser){
      var directive = {
         link       : link,
         restrict   : 'A'
      };
      return directive;
      
      function link(scope, elm, attr){
         scope.user = currentUser;
         console.log(scope);
      }
   }
})();