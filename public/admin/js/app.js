angular.module("app", [])
   .directive("instaUpload", [
      "$http",
      function ($http){
         var dir = {
            require : 'ngModel',
            restrict: 'A',
            link    : link
         };
         return dir;
         function link($scope, $elm, $attrs, model){
            var fileInp = getFileInp($attrs.instaUpload);
            fileInp = angular.element(fileInp);
            $elm.on("click", triggerClick);
            fileInp.on("change", handleNewImageSelected);
            $scope.$on("destroy", function (){
               $elm.off("click", triggerClick);
               fileInp.off("change", handleNewImageSelected);
               fileInp.remove();
            });
            function setValue(val){
               model.$viewValue = val;
               model.$commitViewValue();
               model.$render();
            }
            var selectedFileName;
            function handleNewImageSelected(e){
               if( e.currentTarget.files.length<1){
                  return;
               }
               var file = e.currentTarget.files[0];
               selectedFileName = file.name;
               var reader = new FileReader();
               $elm.prop("disabled","true");
               reader.onload = function(event){
                  var data_url = event.target.result;
                  $http.post(admin_url+"/upload/image",{data_url:data_url})
                     .success(function(response){
                        console.log(response);
                        if(selectedFileName===file.name){
                           setValue(response.file_path)
                        }
                     })
                     .finally(function(){
                        $elm.removeProp("disabled");
                     })
               }
               reader.readAsDataURL(file);
            }

            function triggerClick(){
               fileInp.click();
            }
         }

         function getFileInp(pairId){
            var id = "instaFileInput_" + pairId;
            if ( angular.element("#" + id).length === 0 ) {
               var inp = document.createElement("input");
               inp.type = "file";
               inp.id = id;
               inp.name = "someinputname";
               inp.style.cssText = "display:none;";
               document.body.appendChild(inp);
               return inp;
            }
            return angular.element("#" + id);
         }
      }
   ])
   .directive("questions", [
      function (){
         var dir = {
            restrict: 'A',
            link    : link
         };
         return dir;

         function link($scope, $elm, $attrs){
            if ( $attrs.qText ) {
               $scope.questions = angular.fromJson($attrs.qText);
            } else {
               $scope.questions = [];
            }
            $scope.$watch("questions", function (newValue){
               console.log(newValue);
               $scope.qText = angular.toJson(newValue);
            }, true);
            $scope.normalize_opt_label = function (opt){
               opt.value = opt.label;
            }
            $scope.add_row = function (){
               $scope.questions.push({
                  title      : "",
                  explanation: "",
                  image      : "",
                  options    : []
               });
            }
            $scope.add_option = function (q){
               q.options.push({
                  label: "",
                  image: "",
                  icon : "",
                  add  : 0,
                  minus: 0
               });
            }
            $scope.delete_row = function ($index){
               var temp = angular.copy($scope.questions);
               $scope.questions.length = 0;
               angular.forEach(temp, function (question, index){
                  if ( index !== $index ) {
                     $scope.questions.push(question);
                  }
               });
            }
            $scope.delete_opt = function (q, $index){
               var temp = angular.copy(q.options);
               q.options.length = 0;
               angular.forEach(temp, function (opt, index){
                  if ( index !== $index ) {
                     q.options.push(opt);
                  }
               });
            }
         }
      }
   ]);