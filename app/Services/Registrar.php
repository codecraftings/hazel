<?php namespace App\Services;

use App\Core\User;
use Validator;
use Illuminate\Contracts\Auth\Registrar as RegistrarContract;

class Registrar implements RegistrarContract
{

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validator(array $data)
    {
        //have no functionality. request classes are used
        return Validator::make($data, [
            'name'     => 'sometimes|required|max:255',
            'email'    => 'required|email|max:255|unique:users',
            'password' => 'required|min:5',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    public function create(array $data)
    {
        $user = new User(array_except($data, "password"));
        $user->password = bcrypt($data['password']);
        $user->save();
        return $user;
    }

}
