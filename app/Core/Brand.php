<?php namespace App\Core;

use Illuminate\Database\Eloquent\Model;
/**
 * class Brand
 * @package App\Core
 * @property integer id
 * @property string name
 * @property string logo_url
 * @property DateTime created_at
 * @property DateTime updated_at
 */
class Brand extends Model{
	protected $table = 'brands';
	protected $fillable = ['name'];
	protected $appends = ['logo_url', 'permalink'];
	public function getLogoUrlAttribute(){
		return url('images/brands/' . $this->id . '.png');
	}
	public function getPermalinkAttribute(){
		return url('brands/'.$this->id);
	}
	public function phones(){
		return $this->hasMany('App\Core\Phone');
	}
}
