<?php namespace App\Core;

use Illuminate\Database\Eloquent\Model;

/**
 * class Phone
 * @package App\Core
 * @property integer id
 * @property integer brand_id
 * @property string name
 * @property DateTime created_at
 * @property DateTime updated_at
 * @property string img_url
 */
class Phone extends Model
{
    protected $table = 'phones';
    protected $fillable = ['brand_id', 'name', "base_price", "questions"];
    protected $appends = ['img_url', 'permalink'];
    protected $casts = [
        "questions" => 'array'
    ];

    public function getImgUrlAttribute()
    {
        return url('images/phones/' . $this->id . '.png');
    }

    public function getPermalinkAttribute()
    {
        return url("phones/" . $this->id);
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function rules()
    {
        return $this->hasOne('App\Core\Rule');
    }

    public function getFinalOfferValue($answer_sheet)
    {
        $questions = $this->questions;
        $price = $this->base_price;
        foreach ($questions as $key => $question) {
            if (isset($answer_sheet[$key])) {
                $answer = $answer_sheet[$key];
                if ($answer['title'] == $question['title']) {
                    $selected = $this->getSelectedOption($question['options'], $answer['answer']);
                    if ($selected) {
                        $price += $selected['add'];
                        $price -= $selected['minus'];
                        continue;
                    }
                }
            }
            return 0;
        }
        return $price;
    }

    private function getSelectedOption($options, $answer)
    {
        $selected = null;
        foreach ($options as $option) {
            if ($option['label'] == $answer) {
                $selected = $option;
            }
        }
        return $selected;
    }
}