<?php
namespace App\Core;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    const STATUS_REQUEST_RECEIVED = "request_received";
    const STATUS_AWAITING_REVIEW = "awaiting_review";
    const STATUS_SHIPPING_LABEL_SENT = "shipping_label_sent";
    const STATUS_WAITING_SHIPMENT = "awaiting_shipment";
    const STATUS_SHIPMENT_RECEIVED = "shipment_received";
    const STATUS_PAYMENT_SENT = "payment_sent";
    const STATUS_PAID = "paid";
    protected $table = "orders";
    protected $fillable = ["user_id", "items","total_value","shipping_address","shipping_method","shipment_tracking","payment_method","contact_email", "status"];
    protected $casts = ["shipping_address" => 'array',"payment_method" => 'array',"items"=>"array"];
    protected $appends = ['permalink', 'title', 'img_url'];

    public function getImgUrlAttribute(){
        foreach($this->items as $item){
            return $item['img_url'];
        }
    }
    public function getPermalinkAttribute(){
        return url("offer/".$this->id);
    }
    public function getTitleAttribute(){
        $title = "";
        foreach($this->items as $item){
            $title = $item['name']."+";
        }
        return trim($title, "+");
    }
    public static function isOrderValid($items, $total_value){
        if($total_value<=0){
            return false;
        }
        $calculated_total = 0;
        foreach($items as $item){
            $phone = Phone::find($item['phone_id']);
            $calculated_total += $phone->getFinalOfferValue($item['questions']);
        }
        return $calculated_total==$total_value;
    }
}