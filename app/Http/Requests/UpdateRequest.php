<?php


namespace App\Http\Requests;


use Illuminate\Contracts\Auth\Guard;

class UpdateRequest extends Request {
    /**
     * @var Guard
     */
    private $auth;

    public function __construct(Guard $auth){

        $this->auth = $auth;
    }
    public function rules(){
        return [];
    }
    public function Authorize(){
        return $this->auth->check();
    }
}