<?php


namespace App\Http\Requests;


use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;

class OrderRequest extends Request {
    /**
     * @var Guard
     */
    private $auth;

    public function __construct(Guard $auth)
    {

        $this->auth = $auth;
    }
    public function rules(){
        return [
            "items" => "required",
            "total_value" => "required",
            "shipping_address" => "required",
            "shipping_method" => "required",
            "payment_method" => "required",
            "contact_email" => "required",
            "status" => "sometimes|required",
            "user_id" => "sometimes|required",
        ];
    }
    public function Authorize(){
        return $this->auth->check();
    }
}