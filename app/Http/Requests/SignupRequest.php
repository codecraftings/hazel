<?php


namespace App\Http\Requests;



class SignupRequest extends Request {
    public function rules(){
        return [
            "email" => "required|email",
            "password" => "required|min:3"
        ];
    }
}