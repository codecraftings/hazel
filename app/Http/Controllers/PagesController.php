<?php


namespace App\Http\Controllers;


use Illuminate\Support\Facades\Redirect;

class PagesController extends Controller {
    public function showStaticPages($id){
        try{
            return view("pages.".$id);
        }catch(\Exception $e){
            return Redirect::to('/');
        }
    }
}