<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use Illuminate\Contracts\Filesystem\Factory;
use Illuminate\Support\Facades\Input;

class AdminController extends Controller {
    public function __construct()
    {

    }
    public function showDashboard(){
        return view("admin.dashboard");
    }

    public function uploadImage(Factory $storage){
        $imageData = $this->decodeImage(Input::get("data_url"));
        $file_path = "uploads/images/".uniqid().".png";
        $storage->put($file_path, $imageData);
        return response()->json([
            "file_path" => $file_path
        ]);
    }
    private function decodeImage($imageData)
    {
        if ($pos = strpos($imageData, ",")) {
            $imageData = substr($imageData, $pos + 1);
        }
        $decoded = base64_decode($imageData);
        if (!$decoded)
            return $imageData;
        else
            return $decoded;
    }
}