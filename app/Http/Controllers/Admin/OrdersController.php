<?php


namespace App\Http\Controllers\Admin;


use App\Core\Order;
use Illuminate\Contracts\Filesystem\Factory;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class OrdersController extends AdminController
{
    public function showListingPage()
    {
        $items = Order::query()->latest();
        if(Input::get("status","any")!="any"){
            $items = $items->where("status","=", Input::get("status"));
        }
        $items = $items->paginate(\Input::get('pagging', 10));
        return view("admin.orders.list")->with(compact("items"));
    }

    public function showEditPage($id)
    {
        $order = Order::findOrFail($id);
        return view("admin.orders.edit")->withOrder($order);
    }

    public function submitEditPage($id)
    {
        $order = Order::findOrFail($id);
        $order->update(\Input::all());
        return Redirect::back()->withSuccess("order added!");
    }

    public function submitDelete($id)
    {
        $order = Order::findOrFail($id);
        $order->delete();
        return Redirect::back()->withSuccess("deleted successfully!");
    }
}