<?php


namespace App\Http\Controllers\Admin;


use App\Core\Brand;
use App\Core\Phone;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class PhonesController extends AdminController
{
    public function showListingPage()
    {
        $items = Phone::query()->paginate(\Input::get('pagging', 10));
        return view("admin.phones.list")->with(compact("items"));
    }

    public function showAddPage()
    {
        $brands = $this->getBrandsId();
        return view("admin.phones.add")->with(compact('brands'));
    }

    public function submitAddPage()
    {
        if (!\Input::hasFile("image")) {
            return Redirect::back()->withError("image required");
        }
        $phone = Phone::create($this->getData());
        /** @var UploadedFile $image */
        $image = \Input::file("image");
        $image->move(public_path("images/phones/"), $phone->id . ".png");
        return Redirect::back()->withSuccess("phone added!");
    }

    public function showEditPage($id)
    {
        $phone = Phone::findOrFail($id);
        $brands = $this->getBrandsId();
        return view("admin.phones.edit")->with(compact('phone', 'brands'));
    }

    public function submitEditPage($id)
    {
//        dd(\Input::all());
        $phone = Phone::findOrFail($id);
        $data = $this->getData();
        $phone->update($data);
        if (\Input::hasFile("image")) {
            /** @var UploadedFile $image */
            $image = \Input::file("image");
            $image->move(public_path("images/phones/"), $phone->id . ".png");
        }
        return Redirect::back()->withSuccess("phone updated!");
    }

    public function submitDelete($id)
    {
        $phone = Phone::findOrFail($id);
        $phone->delete();
        return Redirect::back()->withSuccess("deleted successfully!");
    }

    /**
     * @return array|\Illuminate\Database\Eloquent\Collection|static[]
     */
    private function getBrandsId()
    {
        $brands = Brand::all();
        $brands = array_combine(array_pluck($brands, "id"), array_pluck($brands, "name"));
        return $brands;
    }

    /**
     * @return mixed
     */
    private function getData()
    {
        $data = \Input::only("name", "brand_id", "base_price", "questions");
        $data['questions'] = json_decode($data['questions']);
        return $data;
    }
}