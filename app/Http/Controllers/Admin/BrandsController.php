<?php


namespace App\Http\Controllers\Admin;


use App\Core\Brand;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class BrandsController extends AdminController {
    public function showListingPage(){
        $items = Brand::query()->paginate(\Input::get('pagging', 10));
        return view("admin.brands.list")->with(compact("items"));
    }
    public function showAddPage(){
        return view("admin.Brands.add");
    }
    public function submitAddPage(){
        if(!\Input::hasFile("logo")){
            return Redirect::back()->withError("logo required");
        }
        $name = \Input::get("name");
        $brand = Brand::create([
            "name" => $name
        ]);
        /** @var UploadedFile $logo */
        $logo = \Input::file("logo");
        $logo->move(public_path("images/brands/"), $brand->id.".png");
        return Redirect::back()->withSuccess("brand added!");
    }
    public function showEditPage(){

    }
    public function submitEditPage(){

    }
    public function submitDelete($id){
        $brand = Brand::findOrFail($id);
        $brand->delete();
        return Redirect::back()->withSuccess("deleted successfully!");
    }
}