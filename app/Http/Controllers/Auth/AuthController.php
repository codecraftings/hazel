<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\SignupRequest;
use App\Http\Requests\UpdateRequest;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers;

    /**
     * Create a new authentication controller instance.
     *
     * @param  \Illuminate\Contracts\Auth\Guard $auth
     * @param  \Illuminate\Contracts\Auth\Registrar $registrar
     * @return void
     */
    public function __construct(Guard $auth, Registrar $registrar)
    {
        $this->auth = $auth;
        $this->registrar = $registrar;

        $this->middleware('guest', ['except' => ['getLogout', 'submitAjaxLogin', 'submitAjaxSignup','ajaxUpdateUser']]);
    }

    public function submitAjaxLogin(LoginRequest $request)
    {
        $credentials = $request->only('email', 'password');
        if ($this->auth->attempt($credentials, true)) {
            $user = $this->auth->user();
            return response()->json([
                "user" => $user
            ]);
        }
        return response()->json([
            "error" => "Invalid Credentials"
        ], 401);
    }

    public function submitAjaxSignup(SignupRequest $request)
    {
        $data = $request->all();
        try {
            $user = $this->registrar->create($data);
            $this->auth->login($user);
        } catch (\Exception $e) {
            return response()->json([
                "error" => "Something went wrong"
            ], 403);
        }
        return response()->json([
            "user" => $user
        ]);
    }

    public function ajaxUpdateUser(UpdateRequest $request)
    {
        try {
            $user = $this->auth->user();
            $user->update($request->all());
        } catch (\Exception $e) {
            return response()->json([
                "error" => $e->getMessage()
            ], 400);
        }
        return response()->json([
            "user" => $user
        ]);
    }
}
