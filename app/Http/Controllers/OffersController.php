<?php


namespace App\Http\Controllers;


use App\Core\Order;
use App\Core\User;
use App\Http\Requests\OrderRequest;
use Exception;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Request;
use Input;

class OffersController extends Controller
{

    public function showCheckoutPage()
    {
        return view("checkout");
    }

    public function showConfirmationPage()
    {
        return view("confirm");
    }

    public function confirmOrder(OrderRequest $request, Guard $auth)
    {
        $data = Input::all();
        $data['user_id'] = $auth->user()->id;
        $data['status'] = Order::STATUS_AWAITING_REVIEW;
        $data['shipment_tracking'] = "N\A";
        if (!Order::isOrderValid($data['items'], $data['total_value'])) {
            return response()->json([
                "error" => "Order not valid"
            ], 401);
        }
//        dd($data);
//        return response()->json($data);
        $order = Order::create($data);
        return response()->json([
            "id" => $order->id,
            "message" => "Order received!"
        ]);
    }
    public function listUsersOffers(Guard $auth){
        $user = $auth->user();
        $offers = Order::where("user_id","=",$user->id)->latest()->get();
        return view("list-offers")->with(compact("offers"));
    }
    public function showOfferDetails($id, Guard $auth){
        $user = $auth->user();
        $offer = Order::findOrFail($id);
        if($offer->user_id!=$user->id&&$user->user_level!="admin"){
            return response()->json(["error"=> "You are not authorized to view this"], 401);
        }
        return view('offer-page')->withOffer($offer);
    }
    public function checkEmailExists()
    {
        $email = Request::input('email');
        if (!preg_match("/[\S]+@[\S]+\.[A-z0-9]+/", $email)) {
            return response()->json(["error" => "Invalid Email"], 403);
        }
        if (User::where('email', '=', $email)->count() < 1) {
            return response()->json(
                [
                    'email'        => $email,
                    'email_exists' => false
                ]
            );
        } else {
            return response()->json(
                [
                    'email'        => $email,
                    'email_exists' => true
                ]
            );
        }
    }

    public function grabOffer()
    {
        $data = Input::all();
        if (!isset($data['offer']) || !isset($data['user'])) {
            throw new Exception("Invalid Request", 401);
        }
        if (isset($data['loginMode'])) {
            if (!Auth::attempt(array_only($data['user'], ["email", "password"]))) {
                throw new Exception("Invalid credentials!");
            }
            $data['alreadyLoggedIn'] = true;
        }
        if (isset($data['alreadyLoggedIn'])) {
            if (!Auth::check()) {
                throw new Exception("User not logged in!", 403);
            }
            $user = Auth::user();
            $user->update(array_only($data['user'], ["first_name",
                                                     "last_name",
                                                     'address',
                                                     'zip',
                                                     'city',
                                                     'state',
                                                     'country',
                                                     'email'
            ]));
        }
        if (isset($data['registerMode'])) {
            $userData = $data['user'];
            if ($userData['new_password'] != $userData['confirm_new_password']) {
                throw new Exception("Password mismatch!", 402);
            }
            $userData['password'] = Hash::make($userData['new_password']);
            $user = User::create(array_only($data['user'], ["first_name",
                                                            "last_name",
                                                            'address',
                                                            'zip',
                                                            'city',
                                                            'state',
                                                            'country',
                                                            'email',
                                                            'password'
            ]));
            Auth::login($user);;
        }
        return $this->createOffer($data['offer'], $user);
    }

    private function createOffer($data, $user)
    {
        $order = Order::create([
            "user_id" => $user->id,
            "data"    => $data,
            "status"  => Order::STATUS_REQUEST_RECEIVED
        ]);
        return response("done");
    }
}