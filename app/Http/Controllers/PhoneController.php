<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Core\Brand;
use App\Core\Phone;

class PhoneController extends Controller{
	public function showBrandsList(){
		$data = Brand::all();
		return view('brand')->withBrands($data);
	}

	public function showPhonesList( $id ){
		$brand = Brand::find( $id );
		$phones = $brand->phones;
		return view('phone')->with( compact("brand", "phones") );
	}

	public function showRulesList( $id ){
		$phone = Phone::find( $id );
		return view('questions')->withPhone( $phone );
	}
}