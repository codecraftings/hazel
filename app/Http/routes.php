<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index');

Route::get("/pages/{id}",'PagesController@showStaticPages');
Route::post('/email/check', 'OffersController@checkEmailExists');

Route::get("checkout", 'OffersController@showCheckoutPage');
Route::get("checkout/confirm", 'OffersController@showConfirmationPage');
Route::post("checkout/confirm", 'OffersController@confirmOrder');
Route::get("offer/{id}", ['uses'=>'OffersController@showOfferDetails','middleware'=>'auth']);
Route::get("offers", ['uses'=>'OffersController@listUsersOffers','middleware'=>'auth']);

Route::post('/offer/grab', 'OffersController@grabOffer');
Route::controllers([
    'auth'     => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);

Route::post("ajax/login",'Auth\AuthController@submitAjaxLogin');
Route::post("ajax/signup",'Auth\AuthController@submitAjaxSignup');
Route::post("ajax/update",'Auth\AuthController@ajaxUpdateUser');
Route::get('brands', 'PhoneController@showBrandsList');

Route::get('brands/{id}', 'PhoneController@showPhonesList');

Route::get('phones/{id}', 'PhoneController@showRulesList');

Route::group(['prefix' => 'bknd', 'namespace' => 'Admin', 'middleware' => "auth.admin"], function () {
    Route::get('/', "AdminController@showDashboard");
    Route::get('dashboard', "AdminController@showDashboard");
    Route::post("/upload/image", "AdminController@uploadImage");
    function gg($slag, $controller)
    {
        Route::get($slag."/list", $controller."@showListingPage");
        Route::get($slag."/add", $controller."@showAddPage");
        Route::post($slag."/add", $controller."@submitAddPage");
        Route::get($slag."/edit/{id}", $controller."@showEditPage");
        Route::post($slag."/edit/{id}", $controller."@submitEditPage");
        Route::post($slag."/delete/{id}", $controller."@submitDelete");
    }
    gg("brands", "BrandsController");
    gg("phones", "PhonesController");
    gg("orders", "OrdersController");
});

