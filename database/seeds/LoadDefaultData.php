<?php


use App\Core\Brand;
use App\Core\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class LoadDefaultData extends Seeder {
    public function run(){
        Brand::create([
            "name" => "Apple"
        ]);
        Brand::create([
            "name" => "Samsung"
        ]);
        Brand::create([
            "name" => "Blackberry"
        ]);
        Brand::create([
            "name" => "HTC"
        ]);
        Brand::create([
            "name" => "LG"
        ]);
        Brand::create([
            "name" => "SONY"
        ]);
        Brand::create([
            "name" => "ASUS"
        ]);
        Brand::create([
            "name" => "HUAWEI"
        ]);
        User::create([
            "first_name" => "Mahfuz",
            "email" => "mahfuz@gmail.com",
            "user_level" => "admin",
            "password" => Hash::make("bangla")
        ]);
    }
}