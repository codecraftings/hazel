<?php


use App\Core\Phone;
use App\Core\Rule;
use Illuminate\Database\Seeder;

class LoadDemoData extends Seeder
{
    public function run()
    {
        $this->loadPhones();
    }

    private function loadPhones()
    {
        $this->createPhone(1, "Iphone 6 plus");
        $this->createPhone(1, "Iphone 6");
        $this->createPhone(1, "Iphone 5s");
        $this->createPhone(1, "Iphone 5");
        $this->createPhone(1, "Iphone 4s");
    }

    private function createPhone($brand_id, $name, $base_price = 500)
    {
        $phone = Phone::create([
            "brand_id" => $brand_id,
            "name"      => $name,
            "base_price" => $base_price,
            "questions" => [
                $this->getCarrierQ(),
                $this->getColorQ(),
                $this->getSizeQ(),
                $this->getOneQ(),
                $this->getAnotherQ(),
            ]
        ]);
    }
    private function getCarrierQ(){
        return [
            "title"=> "What is the carrier of your device?",
            "explanation" => "Is it locked with a carrier? Or unlocked? Choose from below options.",
            "image" => "images/phones/sample.png",
            "options" => [
                [
                    "label" => "Unlocked",
                    "value" => "unlocked",
                    "image" => "images/networks/1.png",
                    "add" => 0,
                    "minus" => mt_rand(20, 28)
                ],
                [
                    "label" => "Sprint",
                    "value" => "sprint",
                    "image" => "images/networks/2.png",
                    "add" => 0,
                    "minus" => mt_rand(20, 28)
                ],
                [
                    "label" => "T-Mobile",
                    "value" => "t-mobile",
                    "image" => "images/networks/3.png",
                    "add" => 0,
                    "minus" => mt_rand(20, 28)
                ],
                [
                    "label" => "Verizone",
                    "value" => "verizone",
                    "image" => "images/networks/4.png",
                    "add" => 0,
                    "minus" => mt_rand(20, 28)
                ],
                [
                    "label" => "AT&T",
                    "value" => "at&t",
                    "image" => "images/networks/5.png",
                    "add" => 0,
                    "minus" => mt_rand(20, 28)
                ],
            ]
        ];
    }
    private function getColorQ(){
        return [
            "title"=> "What is the color of your device?",
            "explanation" => "What is the primary color of your device. Our price varies slightly depending on the color of the device.",
            "image" => "images/phones/sample.png",
            "options" => [
                [
                    "label" => "Black",
                    "value" => "black",
                    "icon" => "images/colors/black.png",
                    "add" => 0,
                    "minus" => mt_rand(20, 28)
                ],
                [
                    "label" => "White",
                    "value" => "white",
                    "icon" => "images/colors/white.png",
                    "add" => 0,
                    "minus" => mt_rand(20, 28)
                ],
                [
                    "label" => "Blue",
                    "value" => "blue",
                    "icon" => "images/colors/blue.png",
                    "add" => 0,
                    "minus" => mt_rand(20, 28)
                ],
                [
                    "label" => "Yellow",
                    "value" => "yellow",
                    "icon" => "images/colors/yellow.png",
                    "add" => 0,
                    "minus" => mt_rand(20, 28)
                ],
            ]
        ];
    }
    private function getSizeQ(){
        return [
            "title"=> "What is the storage size of your device?",
            "explanation" => "Select the size of your internal storage not external memory sticks.",
            "image" => "images/phones/sample.png",
            "options" => [
                [
                    "label" => "8GB",
                    "value" => "8gb",
                    "add" => 0,
                    "minus" => mt_rand(20, 28)
                ],
                [
                    "label" => "16GB",
                    "value" => "16gb",
                    "add" => 0,
                    "minus" => mt_rand(20, 28)
                ],
                [
                    "label" => "32GB",
                    "value" => "32gb",
                    "add" => 0,
                    "minus" => mt_rand(20, 28)
                ],
                [
                    "label" => "64GB",
                    "value" => "64gb",
                    "add" => 0,
                    "minus" => mt_rand(20, 28)
                ],
            ]
        ];
    }
    private function getOneQ(){
        return [
            "title" => "What is the condition of your screen?",
            "explanation" => "Is it broken? has any scratch or does not have any display. is there any issue with colors",
            "image"=> "images/demo/broken.jpg",
            "options" => [
                [
                    "label" => "Broken",
                    "value" => "broken",
                    "add" => 0,
                    "minus" => mt_rand(20, 28)
                ],
                [
                    "label" => "Good",
                    "value" => "good",
                    "add" => 0,
                    "minus" => mt_rand(8, 12)
                ],
                [
                    "label" => "Flawless",
                    "value" => "flawless",
                    "add" => 0,
                    "minus" => mt_rand(2, 6)
                ]
            ]
        ];
    }
    private function getAnotherQ(){
        return [
            "title" => "Is your home button working?",
            "explanation" => "Does your home button works good? Need hard pressure? Or completely not working?",
            "image"=> "images/demo/home-button.jpg",
            "options" => [
                [
                    "label" => "Not Working",
                    "value" => "not_working",
                    "add" => 0,
                    "minus" => mt_rand(20, 28)
                ],
                [
                    "label" => "Works But Hard",
                    "value" => "hard",
                    "add" => 0,
                    "minus" => mt_rand(8, 12)
                ],
                [
                    "label" => "Flawless",
                    "value" => "flawless",
                    "add" => 0,
                    "minus" => mt_rand(2, 6)
                ]
            ]
        ];
    }
}