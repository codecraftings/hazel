<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $this->emptyTables();
        $this->call('LoadDefaultData');
        if (Config::get("app.debug")) {
            $this->call("LoadDemoData");
        }
    }

    private function emptyTables()
    {
        $tables = DB::select("SHOW TABLES");
        $Tables_in_database = "Tables_in_" . Config::get("database.connections.mysql.database");
        foreach ($tables as $table) {
            if ($table->$Tables_in_database !== "migrations") {
                DB::table($table->$Tables_in_database)->truncate();
            }
        }
    }

}
