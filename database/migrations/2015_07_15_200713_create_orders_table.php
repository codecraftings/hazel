<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create("orders", function(Blueprint $table){
			$table->bigIncrements("id");
			$table->integer("user_id", false, true);
			$table->string("total_value");
			$table->json("items");
			$table->json("shipping_address");
			$table->json("payment_method");
			$table->string("shipping_method");
			$table->string("shipment_tracking");
			$table->string("contact_email");
			$table->string("status");
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop("orders");
	}

}
