<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('first_name');
			$table->string('last_name');
			$table->string('address1');
			$table->string('address2');
			$table->string('city');
			$table->string('zip');
			$table->string('state');
			$table->string('phone');
			$table->string('country');
			$table->json('payment_method');
			$table->string('shipping_method');
			$table->string('email')->unique();
			$table->string('password', 60);
			$table->string('user_level', 20)->default("user");
			$table->rememberToken();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
